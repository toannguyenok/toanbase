# Movie application in Clean Architecture

Movie app demonstrates how I built an Android app with a clean architecture. It displays a map and list of movie base on requirements.
<br>API call from domain https://api.themoviedb.org/3</br>
<br>Because of lack of time so I will not implement function search movie by address or location. (Tet's holiday overlap).</br>
<br>You should use JDK 11 to build application.</br>
## Flow of application:
* When open application, you will see Map first.
* Please grant permission location.
* Marker movie default will display with your current location. Click on Marker will navigate to List move NowPlaying and TopRated.

<br>The project base on my project practice. So please focus on folder <code><b>movies</b></code> on each layer. We also have 3 layers: domain, data, presentation.</br>


## Technical

* [MVVM]
* [Clean Architecture]
* [Dependency Injection]
* [Design pattern]
* [Single - Responsibility Principle] - Usecase practice
* [Reactive Programming in UI]

## Libraries

* [Jetpack]
* [Hilt]
* [OkHttp3]
* [Retrofit]
* [Room]
* [RxJava2]
* [Corountine]

## Structure
* [Domain]
* [Data]
* [Presentation]

We seperate into three layers:Domain is the core layer which contains all logic business. Data layer implements the interface's domain layer. Usecases from the Domain layer are used by the Presentation layer to display data on UI


## Checklist

* [x] Fetch the list of top rated movies
* [x] Fetch the list of now playing movies
* [x] Showing error screen in failure case (by Toast)
* [x] Pull to refresh
* [x] The cell must displayed: poster image, title, rating, total votes, release date 
* [x] Scrolling performance
* [x] Able to request the movie detail information
* [x] Avoid exposing API token
* [x] Fetch the list of movies page by page
* [x] Good amount of coverage for unit test

object Version {
    const val HiltVersion = "2.38.1"
    const val HiltExtAndroidX = "1.0.0-alpha03"

    const val RetrofitVersion = "2.9.0"
    const val OkHttpVersion = "4.9.1"
    const val RoomVersion = "2.2.5"
}

object Hilt {
    const val HiltAndroid = "com.google.dagger:hilt-android:${Version.HiltVersion}"
    const val HiltCompiler = "com.google.dagger:hilt-compiler:${Version.HiltVersion}"
    const val HiltAndroidXCommon = "androidx.hilt:hilt-common:${Version.HiltExtAndroidX}"
    const val HiltAndroidXViewModel =
        "androidx.hilt:hilt-lifecycle-viewmodel:${Version.HiltExtAndroidX}"
    const val HiltAndroidXWorker = "androidx.hilt:hilt-work:${Version.HiltExtAndroidX}"
    const val HiltAndroidXCompiler = "androidx.hilt:hilt-compiler:${Version.HiltExtAndroidX}"
}

object Retrofit {
    const val RetrofitBase = "com.squareup.retrofit2:retrofit:${Version.RetrofitVersion}"
    const val RetrofitGson = "com.squareup.retrofit2:converter-gson:${Version.RetrofitVersion}"
    const val RetrofitAdapterRxjava2 =
        "com.squareup.retrofit2:adapter-rxjava2:${Version.RetrofitVersion}"
}

object OkHttp {
    const val OKHttp = "com.squareup.okhttp3:okhttp:${Version.OkHttpVersion}"
    const val LoggingOkHttp3 = "com.squareup.okhttp3:logging-interceptor:${Version.OkHttpVersion}"
}

object Rx {
    const val RxJava = "io.reactivex.rxjava2:rxjava:2.2.19"
    const val RxAndroid = "io.reactivex.rxjava2:rxandroid:2.1.0"
    const val RxKotlin = "io.reactivex.rxjava2:rxkotlin:2.1.0"
}

object Room {
    const val RoomRuntime = "androidx.room:room-runtime:${Version.RoomVersion}"
    const val RoomCompiler = "androidx.room:room-compiler:${Version.RoomVersion}"
    const val RoomRxJava = "androidx.room:room-rxjava2:${Version.RoomVersion}"
    const val RoomKtx = "androidx.room:room-ktx:${Version.RoomVersion}"
}

object UnitTest {

    private const val junitVersion = "4.12"
    const val junit = "junit:junit:$junitVersion"

    private const val roboelectricVersion = "4.4"
    const val robolectric = "org.robolectric:robolectric:$roboelectricVersion"

    private const val mockitoVersion = "3.11.2"
    const val mockito_inline = "org.mockito:mockito-inline:$mockitoVersion"
    const val mockito_core = "org.mockito:mockito-core:$mockitoVersion"
    const val mockito_kotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:2.1.0"
    const val mockito_kotlin_v3 = "org.mockito.kotlin:mockito-kotlin:4.0.0"

    const val mockk = "io.mockk:mockk:1.12.0"
    const val power_mock = "org.powermock:powermock-mockito-release-full:1.5.4"
}

object AndroidTest {
    const val core_testing = "androidx.arch.core:core-testing:2.1.0"
    const val core = "androidx.test:core:1.1.0"
    const val runner = "androidx.test:runner:1.2.0"
    const val rules = "androidx.test:rules:1.2.0"
    const val junit = "androidx.test.ext:junit:1.1.1"
    const val espressoCore = "androidx.test.espresso:espresso-core:3.2.0"
}
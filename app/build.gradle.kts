plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdkVersion(31)
    defaultConfig {
        applicationId = "com.toan.toanbase"
        minSdk = 21
        targetSdk = 30
        versionCode = 1
        versionName = "1.0"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            isDebuggable = false
            proguardFile(getDefaultProguardFile("proguard-android.txt"))
        }
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
    }
    packagingOptions {
        exclude("META-INF/core_release.kotlin_module")
        exclude("META-INF/library_release.kotlin_module")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    flavorDimensions("app")
    productFlavors {
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    externalNativeBuild {
        cmake {
            path = file("CMakeLists.txt")
        }
    }
}

dependencies {
    implementation("androidx.core:core-ktx:1.6.0")
    implementation("androidx.fragment:fragment-ktx:1.2.4")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.4.0")
    implementation("androidx.appcompat:appcompat:1.3.1")
    implementation("com.google.android.material:material:1.4.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.1")
    implementation("com.google.android.gms:play-services-maps:17.0.1")
    implementation("com.google.android.gms:play-services-location:19.0.1")
    testImplementation("junit:junit:4.+")
    testImplementation(UnitTest.mockito_core)
    testImplementation(UnitTest.mockk)
    testImplementation(UnitTest.mockito_kotlin)
    testImplementation(UnitTest.mockito_kotlin_v3)
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.4.2")
    testImplementation(AndroidTest.core_testing)
    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
    implementation(Hilt.HiltAndroid)
    implementation(Hilt.HiltAndroidXCommon)
    implementation(Hilt.HiltAndroidXWorker)
    kapt(Hilt.HiltCompiler)
    kapt(Hilt.HiltAndroidXCompiler)

    implementation(Retrofit.RetrofitBase)
    implementation(Retrofit.RetrofitGson)
    implementation(Retrofit.RetrofitAdapterRxjava2)

    implementation(OkHttp.OKHttp)
    implementation(OkHttp.LoggingOkHttp3)

    implementation(Rx.RxJava)
    implementation(Rx.RxAndroid)
    implementation(Rx.RxKotlin)

    implementation("com.github.bumptech.glide:glide:4.12.0")
    annotationProcessor("com.github.bumptech.glide:compiler:4.12.0")
    implementation(Room.RoomRuntime)
    kapt(Room.RoomCompiler)
    implementation(Room.RoomRxJava)
    implementation(Room.RoomKtx)

    implementation("com.scottyab:rootbeer-lib:0.0.7")
    implementation("androidx.security:security-crypto:1.0.0")

    implementation("net.zetetic:android-database-sqlcipher:4.5.0")
    implementation("androidx.sqlite:sqlite:2.0.1")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.9")
    compileOnly("com.pinterest.ktlint:ktlint-core:0.34.2")

    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")
}

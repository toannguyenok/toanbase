//
// Created by Nguyen Duc Toan on 1/29/22.
//

#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring

JNICALL
Java_com_toan_toanbase_AppConfigId_movieAPIKey(JNIEnv *env, jobject object) {
    std::string movieAPIKey = "a2c5deb5fdb2ebb10ce53c1fe6b06eca";
    return env->NewStringUTF(movieAPIKey.c_str());
}

package com.toan.toanbase.di

import android.content.Context
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.toan.toanbase.AppConfigId
import com.toan.toanbase.presentation.utils.SystemUtils
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(@ApplicationContext application: Context): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideAppId(@ApplicationContext application: Context): AppConfigId {
        return AppConfigId(application)
    }

    @Provides
    @Singleton
    fun provideSystemUtils(@ApplicationContext application: Context): SystemUtils {
        return SystemUtils(application)
    }

    @Singleton
    @Provides
    @Named("gson_view_model")
    fun provideGson(): Gson {
        return GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
            .setLenient()
            .create()
    }
}

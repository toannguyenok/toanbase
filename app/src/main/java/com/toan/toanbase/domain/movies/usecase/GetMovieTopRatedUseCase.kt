package com.toan.toanbase.domain.movies.usecase

import com.toan.toanbase.domain.movies.MovieRepository
import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import com.toan.toanbase.domain.movies.model.MovieItemListRes
import com.toan.toanbase.domain.movies.model.request.GetMovieListRequest
import com.toan.toanbase.domain.result.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetMovieTopRatedUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) :
    GetMovieListUseCase() {
    override fun execute(parameters: GetMovieListRequest): Flow<Result<BaseMovieListRes<MovieItemListRes>>> {
        return flow {
            emit(Result.Loading)
            val listMovie = movieRepository.getMovieTopRated(parameters)
            val configurationImage = movieRepository.getConfigurationImage()
            listMovie.results?.let {
                for (i in it.indices) {
                    it[i].posterPath = configurationImage.getFullPathPoster(it[i].posterPath)
                }
            }
            emit(Result.Success(listMovie))
        }
    }
}

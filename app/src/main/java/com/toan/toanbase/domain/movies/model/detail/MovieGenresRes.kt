package com.toan.toanbase.domain.movies.model.detail

import com.google.gson.annotations.SerializedName

class MovieGenresRes(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String
)

package com.toan.toanbase.domain.movies.model

import com.google.gson.annotations.SerializedName

class BaseMovieListRes<T>(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: List<T>?,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("total_results") val totalResults: Int,
)

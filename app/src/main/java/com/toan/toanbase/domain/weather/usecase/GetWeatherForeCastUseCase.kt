package com.toan.toanbase.domain.weather.usecase

import com.toan.toanbase.domain.RxUseCase
import com.toan.toanbase.domain.weather.WeatherRepository
import com.toan.toanbase.domain.weather.model.WeatherForecastListRes
import com.toan.toanbase.domain.weather.model.request.GetWeatherForecastRequest
import io.reactivex.Observable
import javax.inject.Inject

class GetWeatherForeCastUseCase @Inject constructor(
    private val weatherRepository: WeatherRepository
) :
    RxUseCase<GetWeatherForecastRequest, WeatherForecastListRes>() {
    override fun execute(parameters: GetWeatherForecastRequest): Observable<WeatherForecastListRes> {
        if (parameters.query.isBlank()) {
            return Observable.fromCallable {
                return@fromCallable WeatherForecastListRes(null, null)
            }
        }
        return weatherRepository.getWeatherForecast(parameters)
    }
}

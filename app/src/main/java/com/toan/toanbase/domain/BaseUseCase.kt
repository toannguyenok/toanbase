package com.toan.toanbase.domain

abstract class BaseUseCase<I, O> {

    protected abstract fun buildUseCase(params: I): O

    fun executeUseCase(params: I): O {
        return buildUseCase(params)
    }
}

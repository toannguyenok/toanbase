package com.toan.toanbase.domain.movies.model.detail

import com.google.gson.annotations.SerializedName

class MovieDetailRes(
    @SerializedName("adult") val adult: Boolean,
    @SerializedName("backdrop_path") val backDropPath: String,
    @SerializedName("genres") val genres: List<MovieGenresRes>
)

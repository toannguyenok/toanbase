package com.toan.toanbase.domain.movies.usecase

import com.toan.toanbase.domain.CoroutineUseCase
import com.toan.toanbase.domain.movies.MovieRepository
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class FetchConfigurationUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) :
    CoroutineUseCase<String, Unit>(Dispatchers.IO) {
    override suspend fun execute(parameters: String) {
        movieRepository.fetchConfiguration(parameters)
    }
}

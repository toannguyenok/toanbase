package com.toan.toanbase.domain.movies

import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import com.toan.toanbase.domain.movies.model.ConfigurationImage
import com.toan.toanbase.domain.movies.model.MovieItemListRes
import com.toan.toanbase.domain.movies.model.detail.MovieDetailRes
import com.toan.toanbase.domain.movies.model.request.GetMovieListRequest

interface MovieRepository {

    suspend fun getMovieNowPlaying(getMovieListRequest: GetMovieListRequest): BaseMovieListRes<MovieItemListRes>

    suspend fun getMovieTopRated(getMovieListRequest: GetMovieListRequest): BaseMovieListRes<MovieItemListRes>

    suspend fun fetchConfiguration(apiKey: String)

    suspend fun getConfigurationImage(): ConfigurationImage

    suspend fun getMovieDetail(apiKey: String, movieId: Int): MovieDetailRes
}

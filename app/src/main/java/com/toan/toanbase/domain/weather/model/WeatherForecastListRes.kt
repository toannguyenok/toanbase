package com.toan.toanbase.domain.weather.model

import com.google.gson.annotations.SerializedName

data class WeatherForecastListRes(
    @SerializedName("cnt") val cnt: Int?,
    @SerializedName("list") var list: List<WeatherForecastDetailRes>?
)

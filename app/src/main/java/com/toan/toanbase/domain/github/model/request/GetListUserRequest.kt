package com.toan.toanbase.domain.github.model.request

class GetListUserRequest(
    val name: String,
    val page: Int,
    val limit: Int
)

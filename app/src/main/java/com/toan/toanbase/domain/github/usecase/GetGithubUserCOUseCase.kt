package com.toan.toanbase.domain.github.usecase

import com.toan.toanbase.domain.CoroutineUseCase
import com.toan.toanbase.domain.github.GithubUserRepository
import com.toan.toanbase.domain.github.model.GithubListUserRes
import com.toan.toanbase.domain.github.model.request.GetListUserRequest
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class GetGithubUserCOUseCase @Inject constructor(
    private val githubUserRepository: GithubUserRepository
) :
    CoroutineUseCase<GetListUserRequest, GithubListUserRes>(Dispatchers.IO) {
    override suspend fun execute(parameters: GetListUserRequest): GithubListUserRes {
        if (parameters.name.isBlank()) {
            // return hint or anythings
            return GithubListUserRes(10, false, null)
        }
        return githubUserRepository.getListUserGithubSuspend(parameters)
    }
}

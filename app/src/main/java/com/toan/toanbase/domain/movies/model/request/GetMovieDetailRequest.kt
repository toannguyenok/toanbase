package com.toan.toanbase.domain.movies.model.request

class GetMovieDetailRequest(
    val apiKey: String,
    val movieId: Int
)

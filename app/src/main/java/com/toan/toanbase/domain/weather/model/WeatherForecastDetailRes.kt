package com.toan.toanbase.domain.weather.model

import com.google.gson.annotations.SerializedName

data class WeatherForecastDetailRes(
    @SerializedName("dt") val dt: Long?,
    @SerializedName("pressure") val pressure: Int?,
    @SerializedName("humidity") val humidity: Int?,
    @SerializedName("temp") val temp: WFTempDetailRes?,
    @SerializedName("weather") val weather: List<WFWeatherDetailItemRes>?,
)

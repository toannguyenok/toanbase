package com.toan.toanbase.domain.movies.model.request

class GetMovieListRequest(
    val apiKey: String,
    val page: Int
)

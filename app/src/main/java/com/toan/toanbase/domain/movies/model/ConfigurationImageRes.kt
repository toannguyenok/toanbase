package com.toan.toanbase.domain.movies.model

import com.google.gson.annotations.SerializedName

class ConfigurationImageRes(
    @SerializedName("base_url") val baseUrl: String?,
    @SerializedName("secure_base_url") val secureBaseUrl: String?,
    @SerializedName("backdrop_sizes") val backdropSizes: List<String>?,
    @SerializedName("poster_sizes") val posterSizes: List<String>?,
)

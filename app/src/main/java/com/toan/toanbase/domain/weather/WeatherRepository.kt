package com.toan.toanbase.domain.weather

import com.toan.toanbase.domain.weather.model.WeatherForecastListRes
import com.toan.toanbase.domain.weather.model.request.GetWeatherForecastRequest
import io.reactivex.Observable

interface WeatherRepository {

    fun getWeatherForecast(request: GetWeatherForecastRequest): Observable<WeatherForecastListRes>
}

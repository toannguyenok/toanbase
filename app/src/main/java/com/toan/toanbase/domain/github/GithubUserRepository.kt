package com.toan.toanbase.domain.github

import com.toan.toanbase.domain.github.model.GithubListUserRes
import com.toan.toanbase.domain.github.model.request.GetListUserRequest
import io.reactivex.Observable

interface GithubUserRepository {

    fun getListUserGithub(request: GetListUserRequest): Observable<GithubListUserRes>

    suspend fun getListUserGithubSuspend(request: GetListUserRequest): GithubListUserRes
}

package com.toan.toanbase.domain

import com.toan.toanbase.domain.result.Result
import io.reactivex.Observable
import io.reactivex.functions.Function

/**
 * Executes business logic in its execute method and keep posting updates to the result as
 * [Result<R>].
 * Handling an exception (emit [Result.Error] to the result) is the subclasses's responsibility.
 */
abstract class RxUseCase<in P, R>() {
    operator fun invoke(parameters: P): Observable<Result<R>> = execute(parameters)
        .map(object : Function<R, Result<R>> {
            override fun apply(t: R): Result<R> {
                return Result.Success(t)
            }
        })
        .onErrorReturn { throwable ->
            return@onErrorReturn Result.Error(throwable)
        }

    protected abstract fun execute(parameters: P): Observable<R>
}

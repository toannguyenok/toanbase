package com.toan.toanbase.domain.movies.usecase

import com.toan.toanbase.domain.FlowUseCase
import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import com.toan.toanbase.domain.movies.model.MovieItemListRes
import com.toan.toanbase.domain.movies.model.request.GetMovieListRequest
import kotlinx.coroutines.Dispatchers

abstract class GetMovieListUseCase :
    FlowUseCase<GetMovieListRequest, BaseMovieListRes<MovieItemListRes>>(Dispatchers.IO)

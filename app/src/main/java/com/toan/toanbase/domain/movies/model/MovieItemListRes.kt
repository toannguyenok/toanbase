package com.toan.toanbase.domain.movies.model

import com.google.gson.annotations.SerializedName

class MovieItemListRes(
    @SerializedName("poster_path") var posterPath: String?,
    @SerializedName("adult") val adult: Boolean,
    @SerializedName("overview") val overView: String?,
    @SerializedName("release_date") val releaseDate: String?,
    @SerializedName("id") val id: Int,
    @SerializedName("original_title") val originalTitle: String?,
    @SerializedName("original_language") val originalLanguage: String?,
    @SerializedName("title") val title: String?,
    @SerializedName("backdrop_path") val backdropPath: String?,
    @SerializedName("popularity") val popularity: Double?,
    @SerializedName("vote_count") val voteCount: Int?,
    @SerializedName("vote_average") val voteAverage: Double?,
)

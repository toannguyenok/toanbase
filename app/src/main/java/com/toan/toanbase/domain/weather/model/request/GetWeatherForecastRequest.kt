package com.toan.toanbase.domain.weather.model.request

class GetWeatherForecastRequest(
    val query: String,
    val cnt: Int,
    val appId: String,
    val units: String = "metric",
    val timeQuery: Long
)

package com.toan.toanbase.domain.weather.model

import com.google.gson.annotations.SerializedName

data class WeatherForecastCityRes(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String?
)

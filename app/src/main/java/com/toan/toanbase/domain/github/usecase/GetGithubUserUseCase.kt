package com.toan.toanbase.domain.github.usecase

import com.toan.toanbase.domain.BaseUseCase
import com.toan.toanbase.domain.github.GithubUserRepository
import com.toan.toanbase.domain.github.model.GithubListUserRes
import com.toan.toanbase.domain.github.model.request.GetListUserRequest
import io.reactivex.Observable
import javax.inject.Inject

class GetGithubUserUseCase @Inject constructor(
    private val githubUserRepository: GithubUserRepository
) :
    BaseUseCase<GetListUserRequest, Observable<GithubListUserRes>>() {
    override fun buildUseCase(params: GetListUserRequest): Observable<GithubListUserRes> {
        if (params.name.isBlank()) {
            // return hint or anythings
            return Observable.fromCallable {
                return@fromCallable GithubListUserRes(10, false, null)
            }
        }
        return githubUserRepository.getListUserGithub(params)
    }
}

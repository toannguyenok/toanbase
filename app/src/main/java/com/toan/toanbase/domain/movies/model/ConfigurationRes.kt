package com.toan.toanbase.domain.movies.model

import com.google.gson.annotations.SerializedName

class ConfigurationRes(@SerializedName("images") val configurationImageRes: ConfigurationImageRes?)

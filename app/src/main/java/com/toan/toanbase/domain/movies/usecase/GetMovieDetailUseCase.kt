package com.toan.toanbase.domain.movies.usecase

import com.toan.toanbase.domain.FlowUseCase
import com.toan.toanbase.domain.movies.MovieRepository
import com.toan.toanbase.domain.movies.model.detail.MovieDetailRes
import com.toan.toanbase.domain.movies.model.request.GetMovieDetailRequest
import com.toan.toanbase.domain.result.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetMovieDetailUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) : FlowUseCase<GetMovieDetailRequest, MovieDetailRes>(Dispatchers.IO) {
    override fun execute(parameters: GetMovieDetailRequest): Flow<Result<MovieDetailRes>> {
        return flow {
            emit(Result.Loading)
            val movieDetailRes =
                movieRepository.getMovieDetail(parameters.apiKey, parameters.movieId)
            emit(Result.Success(movieDetailRes))
        }
    }
}

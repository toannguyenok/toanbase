package com.toan.toanbase.domain.github.model

import com.google.gson.annotations.SerializedName

data class GithubListUserRes(
    @SerializedName("total_count") var totalCount: Long,
    @SerializedName("incomplete_results") var incompleteResults: Boolean,
    @SerializedName("items") var items: List<GithubDetailUserRes>?
)

package com.toan.toanbase.domain.movies.model

class ConfigurationImage(
    private val url: String = "https://image.tmdb.org/t/p/",
    private val backDropSize: String = "original",
    private val posterSize: String = "original"
) {

    fun getFullPathPoster(posterPath: String?): String {
        if (posterPath.isNullOrBlank()) {
            return ""
        }
        return "$url$backDropSize$posterPath"
    }
}

package com.toan.toanbase

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext

class AppConfigId(@ApplicationContext val application: Context) {

    companion object {
        init {
            System.loadLibrary("api_key")
        }
    }
    private external fun movieAPIKey(): String

    fun getAppWeatherAppID() = application.resources.getString(R.string.weather_app_id)

    fun getMovieApiKey() = movieAPIKey()
}

package com.toan.toanbase.data.github.di

import android.content.Context
import com.google.gson.Gson
import com.toan.toanbase.R
import com.toan.toanbase.data.github.remote.GithubService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
@InstallIn(ActivityRetainedComponent::class)
class GithubServiceModule {

    @ActivityRetainedScoped
    @Provides
    fun provideGithubService(@Named("github") retrofit: Retrofit): GithubService =
        retrofit.create(GithubService::class.java)

    @ActivityRetainedScoped
    @Provides
    @Named("github")
    fun provideRetrofit(
        context: Context,
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(context.getString(R.string.endpoint_github))
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @ActivityRetainedScoped
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = (HttpLoggingInterceptor.Level.BODY)
        val builder = OkHttpClient.Builder()

        builder
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .addInterceptor { chain ->
                val requestBuilder = chain.request().newBuilder()
                requestBuilder.addHeader(
                    "Authorization",
                    Credentials.basic("toannd", "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")
                )
                chain.proceed(requestBuilder.build())
            }

        return builder.build()
    }

    companion object {
        const val TIME_OUT = 15L
    }
}

package com.toan.toanbase.data.github.repository

import com.toan.toanbase.data.github.remote.GithubService
import com.toan.toanbase.domain.github.GithubUserRepository
import com.toan.toanbase.domain.github.model.GithubListUserRes
import com.toan.toanbase.domain.github.model.request.GetListUserRequest
import io.reactivex.Observable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GithubUserRepositoryImpl @Inject constructor(private val githubService: GithubService) :
    GithubUserRepository {
    override fun getListUserGithub(request: GetListUserRequest): Observable<GithubListUserRes> {
        return githubService.getUsersList(
            query = request.name + " in:name",
            page = request.page,
            pageSize = request.limit
        )
    }

    override suspend fun getListUserGithubSuspend(request: GetListUserRequest): GithubListUserRes {
        return withContext(Dispatchers.IO) {
            return@withContext githubService.getUsersList2(
                query = request.name + " in:name",
                page = request.page,
                pageSize = request.limit
            )
        }
    }
}

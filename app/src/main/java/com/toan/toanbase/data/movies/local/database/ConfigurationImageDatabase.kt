package com.toan.toanbase.data.movies.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.toan.toanbase.data.ToanBaseSingletonHolder
import com.toan.toanbase.data.movies.local.database.dao.ConfigurationImageDAO
import com.toan.toanbase.data.movies.local.database.entities.ConfigurationImageEntity
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

@Database(
    entities = [
        ConfigurationImageEntity::class
    ],
    version = 1, exportSchema = false
)
@TypeConverters(MovieEntityConverter::class)
abstract class ConfigurationImageDatabase : RoomDatabase() {

    abstract fun configurationImageDAO(): ConfigurationImageDAO

    companion object : ToanBaseSingletonHolder<ConfigurationImageDatabase, Context>({
        Room.databaseBuilder(
            it.applicationContext,
            ConfigurationImageDatabase::class.java,
            "configuration_image.db"
        ).build()
    })

    fun clean(context: Context, compositeDisposable: CompositeDisposable) {
        Completable.fromAction {
            ConfigurationImageDatabase.getInstance(context).clearAllTables()
        }.subscribeOn(Schedulers.io())
            .subscribe {
            }.addTo(compositeDisposable)
    }
}

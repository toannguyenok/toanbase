package com.toan.toanbase.data.weather.di

import com.toan.toanbase.data.weather.local.database.WFDailyDataSource
import com.toan.toanbase.data.weather.local.database.WFDataSourceImpl
import com.toan.toanbase.data.weather.repository.WeatherRepositoryImpl
import com.toan.toanbase.domain.weather.WeatherRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@InstallIn(ActivityRetainedComponent::class)
@Module
abstract class WeatherBindingModule {

    @Binds
    abstract fun provideWeatherRepository(weatherRepository: WeatherRepositoryImpl): WeatherRepository

    @Binds
    abstract fun provideWFDailyDataSource(weatherRepository: WFDataSourceImpl): WFDailyDataSource
}

package com.toan.toanbase.data.utils

import java.util.Calendar

object DateTimeUtil {

    fun getTimeStartOfDay(time: Long): Long {
        val calendar = Calendar.getInstance()
        calendar.apply {
            timeInMillis = time
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        return calendar.timeInMillis
    }

    fun getTimeEndOfDay(time: Long): Long {
        val calendar = Calendar.getInstance()
        calendar.apply {
            timeInMillis = time
            set(Calendar.HOUR_OF_DAY, 23)
            set(Calendar.MINUTE, 59)
            set(Calendar.SECOND, 59)
            set(Calendar.MILLISECOND, 999)
        }
        return calendar.timeInMillis
    }
}

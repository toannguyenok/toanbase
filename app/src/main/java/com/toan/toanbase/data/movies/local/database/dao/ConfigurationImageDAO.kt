package com.toan.toanbase.data.movies.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.toan.toanbase.data.movies.local.database.entities.ConfigurationImageEntity

@Dao
interface ConfigurationImageDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(entity: ConfigurationImageEntity): Long

    @Query("SELECT * FROM ConfigurationImageEntity")
    suspend fun queryConfigurationImageDAO(): List<ConfigurationImageEntity>
}

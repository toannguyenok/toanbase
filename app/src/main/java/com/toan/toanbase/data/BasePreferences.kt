package com.toan.toanbase.data

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import dagger.hilt.android.qualifiers.ApplicationContext

open class BasePreferences constructor(@ApplicationContext context: Context, prefName: String) {

    private val pref: SharedPreferences = BasePreferences.providesSharedPreference(context, prefName)

    companion object {
        fun providesSharedPreference(
            @ApplicationContext context: Context,
            prefName: String
        ): SharedPreferences {
            val sharedPreferences: SharedPreferences

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
                val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)
                sharedPreferences = EncryptedSharedPreferences.create(
                    prefName,
                    mainKeyAlias,
                    context,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
            } else {
                sharedPreferences =
                    context.getSharedPreferences(
                        prefName,
                        Context.MODE_PRIVATE
                    )
            }
            return sharedPreferences
        }
    }

    fun getString(key: String): String? {
        return pref.getString(key, null)
    }

    fun getString(key: String, default: String): String {
        return pref.getString(key, null) ?: default
    }

    fun setString(key: String, value: String) {
        pref.edit().putString(key, value).apply()
    }

    fun getBool(key: String, default: Boolean = false): Boolean {
        return pref.getBoolean(key, default)
    }

    fun setBool(key: String, value: Boolean) {
        pref.edit().putBoolean(key, value).apply()
    }

    fun getInt(key: String, default: Int = 0): Int {
        return pref.getInt(key, default)
    }

    fun setInt(key: String, value: Int) {
        pref.edit().putInt(key, value).apply()
    }

    fun getLong(key: String, default: Long = 0): Long {
        return pref.getLong(key, default)
    }

    fun setLong(key: String, value: Long) {
        pref.edit().putLong(key, value).apply()
    }

    fun remove(key: String) {
        pref.edit().remove(key).apply()
    }

    fun clear() {
        pref.edit().clear().apply()
    }
}

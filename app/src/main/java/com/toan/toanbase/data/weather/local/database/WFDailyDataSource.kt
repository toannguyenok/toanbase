package com.toan.toanbase.data.weather.local.database

import com.toan.toanbase.domain.weather.model.WeatherForecastListRes

interface WFDailyDataSource {

    fun saveWFListResToCache(query: String, weatherForecastListRes: WeatherForecastListRes)

    fun deleteWFDailyFromCache(query: String)

    fun queryWFDailyCache(query: String, cnt: Int, time: Long): WeatherForecastListRes?
}

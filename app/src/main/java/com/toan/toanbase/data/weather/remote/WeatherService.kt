package com.toan.toanbase.data.weather.remote

import com.toan.toanbase.domain.weather.model.WeatherForecastListRes
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {

    @GET("forecast/daily")
    fun getForeCastDaily(
        @Query("q") query: String,
        @Query("cnt") cnt: Int,
        @Query("appid") appId: String,
        @Query("units") units: String
    ): Observable<WeatherForecastListRes>
}

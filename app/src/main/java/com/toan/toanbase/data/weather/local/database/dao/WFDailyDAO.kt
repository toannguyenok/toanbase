package com.toan.toanbase.data.weather.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.toan.toanbase.data.weather.local.database.entities.WFDailyItemEntity

@Dao
interface WFDailyDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(entity: WFDailyItemEntity): Long

    @Query("SELECT * FROM WFDailyItemEntity WHERE query_city = :queryName AND cnt = :cnt AND start_time <= :time AND end_time >= :time ")
    abstract fun queryWFDaily(queryName: String, cnt: Int, time: Long): List<WFDailyItemEntity>

    @Query("DELETE FROM WFDailyItemEntity WHERE query_city = :queryName")
    abstract fun deleteWFDaily(queryName: String)
}

package com.toan.toanbase.data.movies.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.toan.toanbase.domain.movies.model.ConfigurationImageRes

@Entity(tableName = "ConfigurationImageEntity")
class ConfigurationImageEntity internal constructor(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String = "1",

    @ColumnInfo(name = "base_url")
    val baseUrl: String?,

    @ColumnInfo(name = "secure_base_url")
    val secureBaseUrl: String?,

    @ColumnInfo(name = "backdrop_sizes")
    val backdropSizes: List<String>? = null,

    @ColumnInfo(name = "poster_sizes")
    val posterSizes: List<String>? = null,
) {
    companion object {
        fun mapFromConfigurationImageRes(
            configurationImageRes: ConfigurationImageRes,
        ): ConfigurationImageEntity {
            return ConfigurationImageEntity(
                baseUrl = configurationImageRes.baseUrl,
                secureBaseUrl = configurationImageRes.secureBaseUrl,
                backdropSizes = configurationImageRes.backdropSizes,
                posterSizes = configurationImageRes.posterSizes
            )
        }
    }
}

package com.toan.toanbase.data.movies.local.database

import com.toan.toanbase.domain.movies.model.ConfigurationImage
import com.toan.toanbase.domain.movies.model.ConfigurationImageRes

interface ConfigurationImageDataSource {

    suspend fun saveConfigurationImageCache(configurationImageRes: ConfigurationImageRes)

    suspend fun queryConfigurationImageCache(): ConfigurationImage
}

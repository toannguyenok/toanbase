package com.toan.toanbase.data.weather.local.shareRef

import android.content.Context
import com.toan.toanbase.data.BasePreferences
import javax.inject.Inject

class WeatherSharePref @Inject constructor(
    context: Context
) : BasePreferences(context, PREF_NAME) {

    fun setKeyCipherRoom(value: String) {
        setString(KEY_CIPHER_ROOM, value)
    }

    fun getKeyCipherRoom() = getString(KEY_CIPHER_ROOM, "true")

    companion object {
        const val PREF_NAME = "weather_share_pref"
        const val KEY_CIPHER_ROOM = "key_cipher_room"
    }
}

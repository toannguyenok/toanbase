package com.toan.toanbase.data.movies.repository

import com.toan.toanbase.data.movies.local.database.ConfigurationImageDataSource
import com.toan.toanbase.data.movies.remote.MovieService
import com.toan.toanbase.domain.movies.MovieRepository
import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import com.toan.toanbase.domain.movies.model.ConfigurationImage
import com.toan.toanbase.domain.movies.model.MovieItemListRes
import com.toan.toanbase.domain.movies.model.detail.MovieDetailRes
import com.toan.toanbase.domain.movies.model.request.GetMovieListRequest
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val movieService: MovieService,
    private val configurationImageDataSource: ConfigurationImageDataSource
) :
    MovieRepository {
    override suspend fun getMovieNowPlaying(getMovieListRequest: GetMovieListRequest): BaseMovieListRes<MovieItemListRes> {
        return movieService.getMovieNowPlaying(
            getMovieListRequest.apiKey,
            getMovieListRequest.page
        )
    }

    override suspend fun getMovieTopRated(getMovieListRequest: GetMovieListRequest): BaseMovieListRes<MovieItemListRes> {
        return movieService.getMovieTopRated(
            getMovieListRequest.apiKey,
            getMovieListRequest.page
        )
    }

    override suspend fun fetchConfiguration(apiKey: String) {
        val configurationRes = movieService.getConfiguration(apiKey)
        configurationRes.configurationImageRes?.let {
            configurationImageDataSource.saveConfigurationImageCache(it)
        }
    }

    override suspend fun getConfigurationImage(): ConfigurationImage {
        return configurationImageDataSource.queryConfigurationImageCache()
    }

    override suspend fun getMovieDetail(apiKey: String, movieId: Int): MovieDetailRes {
        return movieService.getMovieDetail(apiKey, movieId)
    }
}

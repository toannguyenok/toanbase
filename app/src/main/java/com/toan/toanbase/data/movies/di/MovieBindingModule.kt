package com.toan.toanbase.data.movies.di

import com.toan.toanbase.data.movies.local.database.ConfigurationImageDataSource
import com.toan.toanbase.data.movies.local.database.ConfigurationImageDataSourceImpl
import com.toan.toanbase.data.movies.repository.MovieRepositoryImpl
import com.toan.toanbase.domain.movies.MovieRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@InstallIn(ActivityRetainedComponent::class)
@Module
abstract class MovieBindingModule {

    @Binds
    abstract fun provideMovieRepository(movieRepositoryImpl: MovieRepositoryImpl): MovieRepository

    @Binds
    abstract fun provideConfigurationDataSource(configurationImageDataSourceImpl: ConfigurationImageDataSourceImpl): ConfigurationImageDataSource
}

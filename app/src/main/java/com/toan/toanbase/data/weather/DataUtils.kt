package com.toan.toanbase.data.weather

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object DataUtils {

    internal inline fun <reified T> parseArrayData(
        gson: Gson,
        data: String
    ): T? {
        return try {
            gson.fromJson(data, object : TypeToken<T>() {}.type)
        } catch (e: Exception) {
            null
        }
    }
}

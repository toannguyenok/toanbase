package com.toan.toanbase.data.weather.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.toan.toanbase.data.utils.DateTimeUtil
import com.toan.toanbase.data.weather.DataUtils.parseArrayData
import com.toan.toanbase.domain.weather.model.WeatherForecastDetailRes
import com.toan.toanbase.domain.weather.model.WeatherForecastListRes
import java.util.UUID

@Entity(tableName = "WFDailyItemEntity")
class WFDailyItemEntity internal constructor(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: String = UUID.randomUUID().toString(),

    @ColumnInfo(name = "query_city")
    val query: String,

    @ColumnInfo(name = "cnt")
    val cnt: Int?,

    @ColumnInfo(name = "start_time")
    val startTime: Long? = null,

    @ColumnInfo(name = "end_time")
    val endTime: Long? = null,

    @ColumnInfo(name = "list")
    val list: String? = null
) {
    companion object {
        fun mapFromWFListRes(
            query: String?,
            timeQuery: Long,
            weatherForecastListRes: WeatherForecastListRes,
            gson: Gson
        ): WFDailyItemEntity? {
            if (!query.isNullOrBlank()) {
                val startTime = DateTimeUtil.getTimeStartOfDay(timeQuery)
                val endTime = DateTimeUtil.getTimeEndOfDay(timeQuery)
                var listItemString: String? = null
                weatherForecastListRes.list?.let { items ->
                    try {
                        listItemString = gson.toJson(items)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                return WFDailyItemEntity(
                    query = query.orEmpty(),
                    cnt = weatherForecastListRes.cnt,
                    startTime = startTime,
                    endTime = endTime,
                    list = listItemString
                )
            }
            return null
        }

        fun mapToWeatherForecastListRes(
            entity: WFDailyItemEntity,
            gson: Gson
        ): WeatherForecastListRes? {
            val listItem = try {
                parseArrayData<List<WeatherForecastDetailRes>>(gson, entity.list.orEmpty())
            } catch (ex: Exception) {
                null
            }
            return WeatherForecastListRes(entity.cnt, listItem)
        }
    }
}

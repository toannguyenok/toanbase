package com.toan.toanbase.data.github.remote

import com.toan.toanbase.domain.github.model.GithubDetailUserRes
import com.toan.toanbase.domain.github.model.GithubListUserRes
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {

    @GET("search/users")
    fun getUsersList(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int
    ): Observable<GithubListUserRes>

    @GET("users/{username}")
    fun getUserInfo(
        @Path("username") username: String
    ): Observable<GithubDetailUserRes>

    @GET("search/users")
    suspend fun getUsersList2(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int
    ): GithubListUserRes
}

package com.toan.toanbase.data.weather.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.toan.toanbase.data.ToanBaseSingletonHolder
import com.toan.toanbase.data.weather.local.database.dao.WFDailyDAO
import com.toan.toanbase.data.weather.local.database.entities.WFDailyItemEntity
import com.toan.toanbase.data.weather.local.shareRef.WeatherSharePref
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory
import java.util.UUID

@Database(
    entities = [
        WFDailyItemEntity::class
    ],
    version = 1, exportSchema = false
)
abstract class WeatherForeCastDatabase : RoomDatabase() {

    abstract fun wfDailyDAO(): WFDailyDAO

    companion object : ToanBaseSingletonHolder<WeatherForeCastDatabase, Context>({
        val sharePref = WeatherSharePref(context = it)
        var passPhrase = sharePref.getKeyCipherRoom()
        if (passPhrase.isBlank()) {
            passPhrase = UUID.randomUUID().toString()
            sharePref.setKeyCipherRoom(passPhrase)
        }
        val passphrase: ByteArray = SQLiteDatabase.getBytes(passPhrase.toCharArray())
        val factory = SupportFactory(passphrase)
        Room.databaseBuilder(
            it.applicationContext,
            WeatherForeCastDatabase::class.java,
            "weather_forecast_daily.db"
        ).openHelperFactory(factory).build()
    })

    fun clean(context: Context, compositeDisposable: CompositeDisposable) {
        Completable.fromAction {
            WeatherForeCastDatabase.getInstance(context).clearAllTables()
        }.subscribeOn(Schedulers.io())
            .subscribe {
            }.addTo(compositeDisposable)
    }
}

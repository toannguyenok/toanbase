package com.toan.toanbase.data.weather.local.database

import com.google.gson.Gson
import com.toan.toanbase.data.weather.local.database.dao.WFDailyDAO
import com.toan.toanbase.data.weather.local.database.entities.WFDailyItemEntity
import com.toan.toanbase.domain.weather.model.WeatherForecastListRes
import javax.inject.Inject

class WFDataSourceImpl @Inject constructor(
    private val wfDailyDAO: WFDailyDAO,
    private val gson: Gson
) : WFDailyDataSource {
    override fun saveWFListResToCache(
        query: String,
        weatherForecastListRes: WeatherForecastListRes
    ) {
        val entity = WFDailyItemEntity.mapFromWFListRes(
            query,
            System.currentTimeMillis(),
            weatherForecastListRes, gson
        )
        entity?.let { data ->
            wfDailyDAO.insertAll(data)
        }
    }

    override fun deleteWFDailyFromCache(query: String) {
        wfDailyDAO.deleteWFDaily(query)
    }

    override fun queryWFDailyCache(query: String, cnt: Int, time: Long): WeatherForecastListRes? {
        val entities = wfDailyDAO.queryWFDaily(queryName = query, cnt = cnt, time = time)
        entities.firstOrNull()?.let { item ->
            return WFDailyItemEntity.mapToWeatherForecastListRes(entity = item, gson = gson)
        }
        return null
    }
}

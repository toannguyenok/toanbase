package com.toan.toanbase.data.weather.repository

import com.toan.toanbase.data.weather.local.database.WFDailyDataSource
import com.toan.toanbase.data.weather.remote.WeatherService
import com.toan.toanbase.domain.weather.WeatherRepository
import com.toan.toanbase.domain.weather.model.WeatherForecastListRes
import com.toan.toanbase.domain.weather.model.request.GetWeatherForecastRequest
import io.reactivex.Observable
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(
    private val weatherService: WeatherService,
    private val wfDailyDataSource: WFDailyDataSource
) :
    WeatherRepository {
    override fun getWeatherForecast(request: GetWeatherForecastRequest): Observable<WeatherForecastListRes> {
        return Observable.defer {
            val cache =
                wfDailyDataSource.queryWFDailyCache(request.query, request.cnt, request.timeQuery)
            cache?.list?.let {
                if (it.isNotEmpty()) {
                    return@defer Observable.just(cache)
                }
            }
            return@defer getWeatherForecastFromRemote(request)
        }
    }

    private fun getWeatherForecastFromRemote(request: GetWeatherForecastRequest): Observable<WeatherForecastListRes> {
        return weatherService.getForeCastDaily(
            query = request.query,
            cnt = request.cnt,
            appId = request.appId,
            units = request.units
        ).doOnNext { wfListRes ->
            wfDailyDataSource.deleteWFDailyFromCache(request.query)
            wfDailyDataSource.saveWFListResToCache(request.query, wfListRes)
        }
    }
}

package com.toan.toanbase.data.weather.di

import android.content.Context
import com.google.gson.Gson
import com.toan.toanbase.R
import com.toan.toanbase.data.weather.local.database.WeatherForeCastDatabase
import com.toan.toanbase.data.weather.local.database.dao.WFDailyDAO
import com.toan.toanbase.data.weather.remote.WeatherService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
@InstallIn(ActivityRetainedComponent::class)
class WeatherServiceModule {

    @ActivityRetainedScoped
    @Provides
    fun provideWeatherService(@Named("weather") retrofit: Retrofit): WeatherService =
        retrofit.create(WeatherService::class.java)

    @ActivityRetainedScoped
    @Provides
    @Named("weather")
    fun provideRetrofit(
        context: Context,
        @Named("weather") okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(context.getString(R.string.endpoint_weatherapi))
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @ActivityRetainedScoped
    @Provides
    @Named("weather")
    fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = (HttpLoggingInterceptor.Level.BODY)
        val builder = OkHttpClient.Builder()

        builder
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(interceptor)

        return builder.build()
    }

    @Provides
    @ActivityRetainedScoped
    fun provideWeatherForeCaseDatabase(context: Context): WeatherForeCastDatabase {
        return WeatherForeCastDatabase.getInstance(context)
    }

    @Provides
    @ActivityRetainedScoped
    fun provideWFDailyDAO(db: WeatherForeCastDatabase): WFDailyDAO {
        return db.wfDailyDAO()
    }

    companion object {
        const val TIME_OUT = 15L
    }
}

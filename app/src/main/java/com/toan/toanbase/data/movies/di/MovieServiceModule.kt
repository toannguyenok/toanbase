package com.toan.toanbase.data.movies.di

import android.content.Context
import com.google.gson.Gson
import com.toan.toanbase.R
import com.toan.toanbase.data.movies.local.database.ConfigurationImageDatabase
import com.toan.toanbase.data.movies.local.database.dao.ConfigurationImageDAO
import com.toan.toanbase.data.movies.remote.MovieService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
@InstallIn(ActivityRetainedComponent::class)
class MovieServiceModule {

    @ActivityRetainedScoped
    @Provides
    fun provideMovieService(@Named("movie") retrofit: Retrofit): MovieService =
        retrofit.create(MovieService::class.java)

    @ActivityRetainedScoped
    @Provides
    @Named("movie")
    fun provideRetrofit(
        context: Context,
        @Named("movie") okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(context.getString(R.string.endpoint_movie_api))
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @ActivityRetainedScoped
    @Provides
    @Named("movie")
    fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = (HttpLoggingInterceptor.Level.BODY)
        val builder = OkHttpClient.Builder()

        builder
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(interceptor)

        return builder.build()
    }

    @Provides
    @ActivityRetainedScoped
    fun provideConfigurationImageDatabase(context: Context): ConfigurationImageDatabase {
        return ConfigurationImageDatabase.getInstance(context)
    }

    @Provides
    @ActivityRetainedScoped
    fun provideConfigurationImageDAO(db: ConfigurationImageDatabase): ConfigurationImageDAO {
        return db.configurationImageDAO()
    }

    companion object {
        const val TIME_OUT = 15L
    }
}

package com.toan.toanbase.data.movies.local.database

import com.toan.toanbase.data.movies.local.database.dao.ConfigurationImageDAO
import com.toan.toanbase.data.movies.local.database.entities.ConfigurationImageEntity
import com.toan.toanbase.domain.movies.model.ConfigurationImage
import com.toan.toanbase.domain.movies.model.ConfigurationImageRes
import javax.inject.Inject

class ConfigurationImageDataSourceImpl @Inject constructor(
    private val configurationImageDAO: ConfigurationImageDAO
) : ConfigurationImageDataSource {
    override suspend fun saveConfigurationImageCache(configurationImageRes: ConfigurationImageRes) {
        configurationImageDAO.insertAll(
            ConfigurationImageEntity.mapFromConfigurationImageRes(
                configurationImageRes
            )
        )
    }

    override suspend fun queryConfigurationImageCache(): ConfigurationImage {
        val configurationImageEntity = configurationImageDAO.queryConfigurationImageDAO()
        if (!configurationImageEntity.isNullOrEmpty()) {
            val url = configurationImageEntity[0].secureBaseUrl
            val backDropSize = configurationImageEntity[0].backdropSizes?.let {
                return@let it[it.size / 2]
            }
            val posterSize = configurationImageEntity[0].posterSizes?.let {
                return@let it[it.size / 2]
            }
            if (url != null && backDropSize != null && posterSize != null) {
                return ConfigurationImage(
                    url = url,
                    backDropSize = backDropSize,
                    posterSize = posterSize
                )
            }
        }
        return ConfigurationImage()
    }
}

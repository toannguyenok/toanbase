package com.toan.toanbase.data.movies.remote

import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import com.toan.toanbase.domain.movies.model.ConfigurationRes
import com.toan.toanbase.domain.movies.model.MovieItemListRes
import com.toan.toanbase.domain.movies.model.detail.MovieDetailRes
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET("movie/now_playing")
    suspend fun getMovieNowPlaying(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int
    ): BaseMovieListRes<MovieItemListRes>

    @GET("movie/top_rated")
    suspend fun getMovieTopRated(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int
    ): BaseMovieListRes<MovieItemListRes>

    @GET("configuration")
    suspend fun getConfiguration(
        @Query("api_key") apiKey: String,
    ): ConfigurationRes

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
        @Query("api_key") apiKey: String,
        @Path("movie_id") movieId: Int
    ): MovieDetailRes
}

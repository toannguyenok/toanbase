package com.toan.toanbase.data.github.di

import com.toan.toanbase.data.github.repository.GithubUserRepositoryImpl
import com.toan.toanbase.domain.github.GithubUserRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@InstallIn(ActivityRetainedComponent::class)
@Module
abstract class GithubBindingModule {

    @Binds
    abstract fun provideGithubUserRepository(githubUserRepository: GithubUserRepositoryImpl): GithubUserRepository
}

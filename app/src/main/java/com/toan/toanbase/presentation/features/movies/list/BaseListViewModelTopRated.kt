package com.toan.toanbase.presentation.features.movies.list

import com.google.gson.Gson
import com.toan.toanbase.AppConfigId
import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import com.toan.toanbase.domain.movies.model.MovieItemListRes
import com.toan.toanbase.domain.movies.model.request.GetMovieListRequest
import com.toan.toanbase.domain.movies.usecase.GetMovieTopRatedUseCase
import com.toan.toanbase.domain.result.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class BaseListViewModelTopRated @Inject constructor(
    appConfigId: AppConfigId,
    @Named("gson_view_model") private val gson: Gson,
    private val getMovieTopRatedUseCase: GetMovieTopRatedUseCase
) : BaseListViewModel(appConfigId, gson) {

    override fun getFlowMovieList(getMovieListRequest: GetMovieListRequest): Flow<Result<BaseMovieListRes<MovieItemListRes>>> {
        return getMovieTopRatedUseCase.invoke(getMovieListRequest)
    }
}

package com.toan.toanbase.presentation.features.movies.model.error

import com.google.gson.annotations.SerializedName

open class DataMovieError(
    @SerializedName("status_code") val statusCode: Int? = null,
    @SerializedName("status_message") val statusMessage: String? = null
)

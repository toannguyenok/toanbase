package com.toan.toanbase.presentation.gateway

import android.content.Intent
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.toan.toanbase.presentation.MainActivity
import com.toan.toanbase.presentation.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GatewayActivity : BaseActivity<GatewayViewModel>() {
    private val mViewModel by viewModels<GatewayViewModel>()

    override fun provideViewModel(): GatewayViewModel {
        return mViewModel
    }

    override fun setContentView() {
    }

    override fun setupUI() {
    }

    override fun setupViewModel() {
        provideViewModel().isRootState.observe(
            this,
            Observer { value ->
                if (value) {
                } else {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            }
        )
        provideViewModel().checkRoot()
        provideViewModel().fetchConfiguration()
    }
}

package com.toan.toanbase.presentation.features.weather.model

import com.toan.toanbase.domain.weather.model.WeatherForecastListRes

data class WFDailyUI(val items: List<WFDetailItemUI>?) {
    companion object {
        fun mapFromWFList(weatherForecastListRes: WeatherForecastListRes): WFDailyUI {
            var itemsTemp: MutableList<WFDetailItemUI>? = null
            weatherForecastListRes.list?.let { listItem ->
                if (listItem.isNotEmpty()) {
                    itemsTemp = mutableListOf()
                    listItem.forEach { item ->
                        itemsTemp?.add(WFDetailItemUI.mapFromWFDetail(item))
                    }
                }
            }
            return WFDailyUI(items = itemsTemp)
        }
    }
}

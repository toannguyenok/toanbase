package com.toan.toanbase.presentation.features.movies.model

import com.toan.toanbase.domain.movies.model.MovieItemListRes

data class MovieItemUI(
    val id: Int,
    val title: String?,
    val releaseDate: String?,
    val overView: String?,
    val voteAverage: Double?,
    val posterPath: String?
) {
    companion object {
        fun mapFromMovieItemRes(movieItemListRes: MovieItemListRes): MovieItemUI {
            return MovieItemUI(
                id = movieItemListRes.id,
                title = movieItemListRes.title,
                releaseDate = movieItemListRes.releaseDate,
                overView = movieItemListRes.overView,
                voteAverage = movieItemListRes.voteAverage,
                posterPath = movieItemListRes.posterPath
            )
        }
    }
}

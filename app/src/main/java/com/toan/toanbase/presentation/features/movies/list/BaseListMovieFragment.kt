package com.toan.toanbase.presentation.features.movies.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.toan.toanbase.databinding.FragmentBaseMovieListBinding
import com.toan.toanbase.presentation.base.BaseFragment
import com.toan.toanbase.presentation.features.movies.detail.MovieDetailActivity
import com.toan.toanbase.presentation.features.movies.list.recycleview.MovieAdapterLoadMoreListener
import com.toan.toanbase.presentation.features.movies.list.recycleview.MovieListAdapter
import com.toan.toanbase.presentation.features.movies.model.MovieItemUI
import com.toan.toanbase.presentation.features.movies.model.MovieListUIState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class BaseListMovieFragment : BaseFragment<BaseListViewModel>() {

    companion object {
        private const val EXTRA_TYPE_LIST = "extra_type_list"
        fun newInstance(@MovieListType type: Int): Fragment {
            val argument = Bundle()
            argument.putInt(EXTRA_TYPE_LIST, type)
            val fragment = BaseListMovieFragment()
            fragment.arguments = argument
            return fragment
        }
    }

    override fun provideViewModel(): BaseListViewModel {
        val type = arguments?.getInt(EXTRA_TYPE_LIST, MovieListType.NOW_PLAYING)
            ?: MovieListType.NOW_PLAYING
        return MovieListFactory.getViewModelByType(this, type)
    }

    private lateinit var binding: FragmentBaseMovieListBinding
    private lateinit var movieListAdapter: MovieListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBaseMovieListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun setupUI() {
        setUpRecycleViewListMovie()
        binding.refreshLayout.setOnRefreshListener {
            provideViewModel().refreshMovieList()
        }
    }

    private fun setUpRecycleViewListMovie() {
        binding.rvMovieList.layoutManager = LinearLayoutManager(requireContext())
        movieListAdapter = MovieListAdapter(itemClickCallback)
        binding.rvMovieList.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        binding.rvMovieList.adapter = movieListAdapter
        movieListAdapter.addLoadMoreListener(object : MovieAdapterLoadMoreListener {
            override fun onLoadMore() {
                provideViewModel().loadMore()
            }
        })
    }

    override fun setupViewModel() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                provideViewModel().uiStateListMovie.collect { state ->
                    when (state) {
                        is MovieListUIState.ShowList -> {
                            binding.refreshLayout.isRefreshing = false
                            if (state.data.page == 1) {
                                movieListAdapter.submitList(state.data.items)
                            } else {
                                val oldList = movieListAdapter.currentList as List<MovieItemUI>
                                var newList = oldList
                                state.data.items?.let {
                                    newList = newList + it
                                }
                                movieListAdapter.submitList(newList)
                            }
                        }
                        is MovieListUIState.Loading -> {
                            // show loading.
                            // i will show swipe refresh layout indicator
                            binding.refreshLayout.isRefreshing = true
                        }
                        is MovieListUIState.Error -> {
                            // show toast for error
                            // you can show what ever you want
                            binding.refreshLayout.isRefreshing = false
                            Toast.makeText(
                                requireContext(),
                                state.movieError.getMessage(requireContext()),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }
        provideViewModel().refreshMovieList()
    }

    private val itemClickCallback = fun(movieItemUI: MovieItemUI) {
        MovieDetailActivity.newActivity(this, movieItemUI.id)
    }
}

package com.toan.toanbase.presentation.features.movies.model

import com.toan.toanbase.presentation.features.movies.model.error.MovieError

sealed class MovieListUIState {

    object Loading : MovieListUIState()

    class Error(val movieError: MovieError) : MovieListUIState()

    class ShowList(val data: MovieListItemUI) : MovieListUIState()
}

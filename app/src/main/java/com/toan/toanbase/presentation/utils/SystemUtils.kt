package com.toan.toanbase.presentation.utils

import android.content.Context
import com.scottyab.rootbeer.RootBeer
import javax.inject.Inject

class SystemUtils @Inject constructor(val context: Context) {

    fun isRooted() = RootBeer(context).isRootedWithoutBusyBoxCheck
}

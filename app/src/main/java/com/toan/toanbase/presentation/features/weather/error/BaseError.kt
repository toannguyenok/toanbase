package com.toan.toanbase.presentation.features.weather.error

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class BaseError<P : BaseError.Param>(
    @SerializedName("meta")
    protected val meta: P? = null
) {

    val param: P?
        get() = meta

    companion object {
        const val NETWORK_CODE = 0x111
        const val DATA_CODE = 0x222
        const val HTTP_CODE = 0x333
        const val UNKNOWN_ERROR_CODE = 0x999
    }

    open class Param(
        @SerializedName("code")
        open val code: Int? = UNKNOWN_ERROR_CODE,
        @SerializedName("message")
        open val message: String? = null
    ) : Serializable

    fun isNetworkError(): Boolean {
        return this.param?.code == NETWORK_CODE
    }
}

package com.toan.toanbase.presentation.features.movies.model.error

import android.content.Context
import com.toan.toanbase.R

open class MovieError(var errorCode: Int) : DataMovieError() {
    companion object {
        const val NETWORK_CODE = 0x111
        const val DATA_CODE = 0x222
        const val HTTP_CODE = 0x333
        const val UNKNOWN_ERROR_CODE = 0x999
    }

    fun getMessage(context: Context): String {
        statusMessage?.let {
            return statusMessage
        }
        if (errorCode == NETWORK_CODE) {
            return context.getString(R.string.network_error)
        }
        return ""
    }
}

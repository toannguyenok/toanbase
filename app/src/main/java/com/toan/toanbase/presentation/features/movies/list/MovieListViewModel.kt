package com.toan.toanbase.presentation.features.movies.list

import com.toan.toanbase.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieListViewModel @Inject constructor() :
    BaseViewModel()

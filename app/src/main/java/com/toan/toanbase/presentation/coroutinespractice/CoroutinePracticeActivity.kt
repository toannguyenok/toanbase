package com.toan.toanbase.presentation.coroutinespractice

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.toan.toanbase.databinding.ActivityCoroutinePracticeBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class CoroutinePracticeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCoroutinePracticeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCoroutinePracticeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        testCoroutines()
        Log.e("Toan", "End oncreate")
    }

    private fun testCoroutines() {

        runBlocking {
            launch {
                delay(2000)
                Log.e("Toan", "launch")
            }
            Log.e("Toan", "runblocking")
        }
    }
}

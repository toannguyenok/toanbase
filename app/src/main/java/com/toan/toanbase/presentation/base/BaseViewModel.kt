package com.toan.toanbase.presentation.base

import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.toan.toanbase.presentation.features.movies.model.error.MovieError
import com.toan.toanbase.presentation.features.movies.model.error.MovieErrorAppFactory
import com.toan.toanbase.presentation.features.weather.error.BaseError
import com.toan.toanbase.presentation.features.weather.error.ErrorAppFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel() {

    private var compositeDisposable: CompositeDisposable? = null

    fun addDisposable(disposable: Disposable) {
        if (compositeDisposable == null || compositeDisposable?.isDisposed == true) {
            compositeDisposable = CompositeDisposable()
        }
        compositeDisposable?.add(disposable)
    }

    fun dispose() {
        compositeDisposable?.dispose()
    }

    protected open fun parseError(throwable: Throwable?): BaseError<out BaseError.Param> {
        return ErrorAppFactory.parseError(throwable)
    }

    protected open fun parseMovieError(throwable: Throwable?, gson: Gson): MovieError {
        return MovieErrorAppFactory.parseError(throwable, gson)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable?.dispose()
    }
}

package com.toan.toanbase.presentation.features.movies.list.recycleview

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.toan.toanbase.databinding.ItemMovieBinding
import com.toan.toanbase.presentation.features.movies.model.MovieItemUI

class MovieListViewHolder(
    view: View,
    private val itemMovieBinding: ItemMovieBinding
) :
    RecyclerView.ViewHolder(view) {

    fun bindView(movieItemUI: MovieItemUI) {
        itemMovieBinding.tvTitle.text = movieItemUI.title.orEmpty()
        itemMovieBinding.tvOverView.text = movieItemUI.overView.orEmpty()
        itemMovieBinding.tvReleaseDate.text = movieItemUI.releaseDate.orEmpty()
        itemMovieBinding.tvRatingAverage.text = String.format("%.1f", movieItemUI.voteAverage)
        Glide.with(itemView.context).load(movieItemUI.posterPath.orEmpty()).into(itemMovieBinding.ivBackdrop)
    }
}

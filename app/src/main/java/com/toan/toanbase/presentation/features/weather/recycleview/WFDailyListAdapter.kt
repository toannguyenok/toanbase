package com.toan.toanbase.presentation.features.weather.recycleview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.toan.toanbase.R
import com.toan.toanbase.presentation.features.weather.model.WFDetailItemUI

class WFDailyListAdapter() :
    ListAdapter<WFDetailItemUI, WFDailyViewHolder>(WFDaiLyItemDiffCallback()) {

    private var layoutManager: RecyclerView.LayoutManager? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        layoutManager = recyclerView.layoutManager
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        layoutManager = null
        super.onDetachedFromRecyclerView(recyclerView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WFDailyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_weather_daily, parent, false)
        return WFDailyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WFDailyViewHolder, position: Int) {
        holder.bindView(getItem(position))
    }
}

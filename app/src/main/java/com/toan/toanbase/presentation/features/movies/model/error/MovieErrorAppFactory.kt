package com.toan.toanbase.presentation.features.movies.model.error

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object MovieErrorAppFactory {

    fun parseError(throwable: Throwable?, gson: Gson): MovieError {
        when (throwable) {
            is UnknownHostException, is SocketTimeoutException, is IOException -> {
                return MovieError(MovieError.NETWORK_CODE)
            }
            is JsonSyntaxException -> {
                return MovieError(MovieError.DATA_CODE)
            }
            is HttpException -> {
                val error = gson.fromJson(
                    throwable.response()?.errorBody()?.string(),
                    MovieError::class.java
                )
                error.errorCode = MovieError.HTTP_CODE
                return error
            }
            else -> {
                return MovieError(MovieError.UNKNOWN_ERROR_CODE)
            }
        }
    }
}

package com.toan.toanbase.presentation.features.movies.list.recycleview

interface MovieAdapterLoadMoreListener {

    fun onLoadMore()
}

package com.toan.toanbase.presentation.features.gituser

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.toan.toanbase.domain.github.model.request.GetListUserRequest
import com.toan.toanbase.domain.github.usecase.GetGithubUserCOUseCase
import com.toan.toanbase.domain.github.usecase.GetGithubUserUseCase
import com.toan.toanbase.domain.result.Result
import com.toan.toanbase.presentation.base.BaseViewModel
import com.toan.toanbase.presentation.features.gituser.model.ErrorState
import com.toan.toanbase.presentation.features.gituser.model.GitUserListUI
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class GitUserSearchViewModel @Inject constructor(
    private val getGithubUserUseCase: GetGithubUserUseCase,
    private val getGithubUserCOUseCase: GetGithubUserCOUseCase
) :
    BaseViewModel() {

    private val subjectQueryString: PublishSubject<String> = PublishSubject.create()

    private var disposeSearch: Disposable? = null

    private val _listQueryUser = MutableLiveData<GitUserListUI>()

    private val _errorState = MutableLiveData<ErrorState>()

    var listQueryUser: MutableLiveData<GitUserListUI>
        get() = _listQueryUser
        set(value) {
        }

    var errorState: MutableLiveData<ErrorState>
        get() = _errorState
        set(value) {
        }

    private fun getGitHubUser(query: String) {
//        val request = GetListUserRequest(name = query, page = 0, limit = 50)
//        getGithubUserUseCase.executeUseCase(request)
//            .map { data -> return@map GitUserListUI.mapFromGitListUserRes(data) }
//            .compose(RxUtils.applyApiSchedulers())
//            .subscribe(
//                {
//                    _listQueryUser.postValue(it)
//                },
//                {
//                    //handle error
//                    _errorState.postValue(ErrorState.UnknownError)
//                }
//            ).add(this)
        testCoroutines(query)
    }

    fun initSearch() {
        disposeSearch = subjectQueryString
            .debounce(150, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .distinctUntilChanged()
            .subscribe(
                { result ->
                    searchUser(result)
                },
                {}
            )
    }

    fun search(query: String) {
        subjectQueryString.onNext(query)
    }

    private fun searchUser(input: String) {
        getGitHubUser(input)
    }

    override fun onCleared() {
        disposeSearch?.dispose()
        subjectQueryString.onComplete()
        super.onCleared()
    }

    fun testCoroutines(query: String) {
        viewModelScope.launch {
            val request = GetListUserRequest(name = query, page = 0, limit = 50)
            val result = getGithubUserCOUseCase.invoke(request)
            when (result) {
                is Result.Success -> {
                    Log.e("Toan", "Success with ${result.data}")
                }
                is Result.Error -> {
                    Log.e("Toan", "Error with ${result.throwable?.message}")
                }
            }
        }
    }
}

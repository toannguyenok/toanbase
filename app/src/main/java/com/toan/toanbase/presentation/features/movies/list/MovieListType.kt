package com.toan.toanbase.presentation.features.movies.list

import androidx.annotation.IntDef
import com.toan.toanbase.presentation.features.movies.list.MovieListType.Companion.NOW_PLAYING
import com.toan.toanbase.presentation.features.movies.list.MovieListType.Companion.TOP_RATED

@IntDef(NOW_PLAYING, TOP_RATED)
@Retention(AnnotationRetention.SOURCE)
annotation class MovieListType {
    companion object {
        const val NOW_PLAYING = 0
        const val TOP_RATED = 1
    }
}

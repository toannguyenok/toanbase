package com.toan.toanbase.presentation.features.gituser.model

sealed class ErrorState {
    object NetworkError : ErrorState()

    class GithubError(error: GithubError?) : ErrorState()

    object UnknownError : ErrorState()
}

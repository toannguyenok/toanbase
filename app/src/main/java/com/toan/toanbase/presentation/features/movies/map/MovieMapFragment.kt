package com.toan.toanbase.presentation.features.movies.map

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.toan.toanbase.R
import com.toan.toanbase.databinding.FragmentMovieMapBinding
import com.toan.toanbase.presentation.base.BaseFragment
import com.toan.toanbase.presentation.features.movies.list.MovieListActivity
import com.toan.toanbase.presentation.utils.awaitLastLocation
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MovieMapFragment : BaseFragment<MovieMapViewModel>(), OnMapReadyCallback {

    private val mViewModel by viewModels<MovieMapViewModel>()

    companion object {
        fun newInstance(): Fragment {
            return MovieMapFragment()
        }
    }

    override fun provideViewModel(): MovieMapViewModel {
        return mViewModel
    }

    private lateinit var binding: FragmentMovieMapBinding
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieMapBinding.inflate(inflater, container, false)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        return binding.root
    }

    override fun setupUI() {
        setupMap()
    }

    private fun setupMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(com.toan.toanbase.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun setupViewModel() {
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setOnInfoWindowClickListener {
            MovieListActivity.newActivity(this)
        }
        checkAvailablePermissionAndAction()
    }

    private fun checkAvailablePermissionAndAction() {
        if (!::map.isInitialized) return
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            enableMyLocation()
            animateCurrentLocation()
        } else {
            requestPermissionLauncher.launch(
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    private fun enableMyLocation() {
        map.isMyLocationEnabled = true
    }

    private fun animateCurrentLocation() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                val location = fusedLocationClient.awaitLastLocation()
                map.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            location.latitude,
                            location.longitude
                        ),
                        16.0f
                    )
                )
                map.addMarker(
                    MarkerOptions()
                        .position(
                            LatLng(
                                location.latitude,
                                location.longitude
                            )
                        )
                        .title(requireContext().getString(R.string.marker_title_temp))
                )
            }
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                checkAvailablePermissionAndAction()
            } else {
                val toast = Toast.makeText(
                    requireContext(),
                    requireContext().resources.getString(R.string.location_permission_warning),
                    Toast.LENGTH_LONG
                )
                toast.show()
            }
        }
}

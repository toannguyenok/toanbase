package com.toan.toanbase.presentation.features.weather.model

import com.toan.toanbase.presentation.features.weather.error.BaseError

sealed class WFDailySearchUIState {
    object Empty : WFDailySearchUIState()

    class Error(val baseError: BaseError<out BaseError.Param>) : WFDailySearchUIState()

    class ShowList(val data: WFDailyUI) : WFDailySearchUIState()
}

package com.toan.toanbase.presentation.features.movies.list

import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.toan.toanbase.AppConfigId
import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import com.toan.toanbase.domain.movies.model.MovieItemListRes
import com.toan.toanbase.domain.movies.model.request.GetMovieListRequest
import com.toan.toanbase.domain.result.Result
import com.toan.toanbase.presentation.base.BaseViewModel
import com.toan.toanbase.presentation.features.movies.model.MovieListItemUI
import com.toan.toanbase.presentation.features.movies.model.MovieListUIState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit
import javax.inject.Named

abstract class BaseListViewModel constructor(
    private val appConfigId: AppConfigId,
    @Named("gson_view_model") private val gson: Gson
) : BaseViewModel() {

    private var currentPage = 1
    private var _totalPage = Int.MAX_VALUE
    private var isLoading = false

    private val _uiStateListMovie =
        MutableStateFlow<MovieListUIState>(MovieListUIState.ShowList(MovieListItemUI.emptyItem()))
    val uiStateListMovie: StateFlow<MovieListUIState> = _uiStateListMovie

    private val subjectLoadMore: PublishSubject<Boolean> = PublishSubject.create()
    private var disposeLoadMore: Disposable? = null

    init {
        initLoadMore()
    }

    fun refreshMovieList() {
        currentPage = 1
        getMovieList(currentPage)
    }

    fun loadMore() {
        subjectLoadMore.onNext(true)
    }

    abstract fun getFlowMovieList(getMovieListRequest: GetMovieListRequest): Flow<Result<BaseMovieListRes<MovieItemListRes>>>

    private fun getMovieList(page: Int) {
        isLoading = true
        viewModelScope.launch {
            val listMovieRequest =
                GetMovieListRequest(apiKey = appConfigId.getMovieApiKey(), page = page)
            getFlowMovieList(listMovieRequest)
                .map { state ->
                    when (state) {
                        is Result.Success -> {
                            currentPage = state.data.page
                            _totalPage = state.data.totalPages
                            return@map MovieListUIState.ShowList(
                                MovieListItemUI.mapFromMovieList(
                                    state.data
                                )
                            )
                        }
                        is Result.Error -> {
                            return@map MovieListUIState.Error(
                                parseMovieError(
                                    state.throwable,
                                    gson
                                )
                            )
                        }
                        is Result.Loading -> return@map MovieListUIState.Loading
                    }
                }
                .flowOn(Dispatchers.Default)
                .collect { state ->
                    isLoading = false
                    _uiStateListMovie.value = state
                }
        }
    }

    private fun initLoadMore() {
        disposeLoadMore = subjectLoadMore
            .throttleFirst(200, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    getMovieList(page = currentPage + 1)
                },
                {}
            )
    }

    override fun onCleared() {
        disposeLoadMore?.dispose()
        subjectLoadMore.onComplete()
        super.onCleared()
    }
}

package com.toan.toanbase.presentation.features.movies.list.recycleview

import androidx.recyclerview.widget.DiffUtil
import com.toan.toanbase.presentation.features.movies.model.MovieItemUI

class MovieListItemDiffCallback : DiffUtil.ItemCallback<MovieItemUI>() {

    override fun areItemsTheSame(oldItem: MovieItemUI, newItem: MovieItemUI): Boolean {
        return (oldItem.id == newItem.id)
    }

    override fun areContentsTheSame(oldItem: MovieItemUI, newItem: MovieItemUI): Boolean {
        return oldItem.hashCode() == newItem.hashCode()
    }
}

package com.toan.toanbase.presentation.features.movies.detail

import com.toan.toanbase.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor() :
    BaseViewModel()

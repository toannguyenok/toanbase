package com.toan.toanbase.presentation.features.weather

import androidx.lifecycle.MutableLiveData
import com.toan.toanbase.AppConfigId
import com.toan.toanbase.domain.result.Result
import com.toan.toanbase.domain.weather.model.request.GetWeatherForecastRequest
import com.toan.toanbase.domain.weather.usecase.GetWeatherForeCastUseCase
import com.toan.toanbase.presentation.base.BaseViewModel
import com.toan.toanbase.presentation.features.weather.model.WFDailySearchUIState
import com.toan.toanbase.presentation.features.weather.model.WFDailyUI
import com.toan.toanbase.presentation.utils.RxUtils
import com.toan.toanbase.presentation.utils.add
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class WeatherSearchViewModel @Inject constructor(
    private val getWeatherForeCastUseCase: GetWeatherForeCastUseCase,
    private val appConfigId: AppConfigId
) :
    BaseViewModel() {

    companion object {
        const val CONDITION_SEARCH_QUERY = 3
    }

    private val subjectQueryString: PublishSubject<String> = PublishSubject.create()

    private var disposeSearch: Disposable? = null

    private val _searchUIState = MutableLiveData<WFDailySearchUIState>()

    var searchUIState: MutableLiveData<WFDailySearchUIState>
        get() = _searchUIState
        set(value) {
        }

    private fun getWFDaily(query: String) {
        val request = GetWeatherForecastRequest(
            query = query,
            cnt = 7,
            appId = appConfigId.getAppWeatherAppID(),
            timeQuery = System.currentTimeMillis()
        )
        getWeatherForeCastUseCase.invoke(request)
            .map { t ->
                when (t) {
                    is Result.Success -> Result.Success(WFDailyUI.mapFromWFList(t.data))
                    is Result.Error -> Result.Error(t.throwable)
                    else -> Result.Loading
                }
            }
            .compose(RxUtils.applyApiSchedulers())
            .subscribe(
                { result ->
                    when (result) {
                        is Result.Success -> {
                            if (result.data.items.isNullOrEmpty()) {
                                _searchUIState.postValue(WFDailySearchUIState.Empty)
                            } else {
                                _searchUIState.postValue(WFDailySearchUIState.ShowList(result.data))
                            }
                        }
                        is Result.Error -> {
                            result.throwable?.let { throwable ->
                                val error = parseError(throwable)
                                _searchUIState.postValue(WFDailySearchUIState.Error(error))
                            }
                        }
                        else -> {}
                    }
                },
                { }
            )
            .add(this)
    }

    fun initSearch() {
        disposeSearch = subjectQueryString
            .debounce(200, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    searchWFDaily(result)
                },
                {}
            )
    }

    fun search(query: String) {
        if (query.length >= CONDITION_SEARCH_QUERY) {
            subjectQueryString.onNext(query)
        }
    }

    private fun searchWFDaily(input: String) {
        getWFDaily(input)
    }

    override fun onCleared() {
        disposeSearch?.dispose()
        subjectQueryString.onComplete()
        super.onCleared()
    }
}

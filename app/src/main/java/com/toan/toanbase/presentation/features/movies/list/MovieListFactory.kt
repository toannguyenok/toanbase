package com.toan.toanbase.presentation.features.movies.list

import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels

object MovieListFactory {

    fun getViewModelByType(fragment: Fragment, @MovieListType type: Int): BaseListViewModel {
        when (type) {
            MovieListType.NOW_PLAYING -> return fragment.viewModels<BaseListViewModelNowPlaying>().value
            MovieListType.TOP_RATED -> return fragment.viewModels<BaseListViewModelTopRated>().value
        }
        return fragment.viewModels<BaseListViewModelNowPlaying>().value
    }
}

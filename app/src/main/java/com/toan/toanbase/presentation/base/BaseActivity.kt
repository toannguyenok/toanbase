package com.toan.toanbase.presentation.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity<T : BaseViewModel> : AppCompatActivity() {

    abstract fun provideViewModel(): T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView()
        setupUI()
        setupViewModel()
    }

    abstract fun setContentView()

    abstract fun setupUI()

    abstract fun setupViewModel()
}

package com.toan.toanbase.presentation.features.movies.detail

import android.content.Intent
import android.util.Log
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import com.toan.toanbase.databinding.ActivityMovieDetailBinding
import com.toan.toanbase.presentation.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailActivity : BaseActivity<MovieDetailViewModel>() {

    companion object {
        private const val EXTRA_MOVIE_ID = "extra_movie_id"
        fun newActivity(fragment: Fragment, movieId: Int) {
            fragment.startActivity(
                Intent(
                    fragment.requireContext(),
                    MovieDetailActivity::class.java
                ).apply {
                    putExtra(EXTRA_MOVIE_ID, movieId)
                }
            )
        }
    }

    private val mViewModel by viewModels<MovieDetailViewModel>()

    override fun provideViewModel(): MovieDetailViewModel {
        return mViewModel
    }

    private lateinit var binding: ActivityMovieDetailBinding

    override fun setContentView() {
        binding = ActivityMovieDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun setupUI() {
        val movieId = intent.getIntExtra(EXTRA_MOVIE_ID, -1)
        Log.e("Toan", "movieid $movieId")
    }

    override fun setupViewModel() {
    }
}

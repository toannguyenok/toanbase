package com.toan.toanbase.presentation.features.gituser.recycleview

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.toan.toanbase.R
import com.toan.toanbase.presentation.features.gituser.model.GitUserDetailItemUI

class GithubListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val imgProfile = view.findViewById<AppCompatImageView>(R.id.imgProfile)
    private val tvName = view.findViewById<AppCompatTextView>(R.id.tvName)
    private val tvGithubLink = view.findViewById<AppCompatTextView>(R.id.tvGithubLink)

    fun bindView(gitUserDetailItemUI: GitUserDetailItemUI) {
        tvName.text = gitUserDetailItemUI.name.orEmpty()
        tvGithubLink.text = gitUserDetailItemUI.githubLink.orEmpty()
        Glide.with(itemView.context).load(gitUserDetailItemUI.profileUrl)
            .into(imgProfile)
    }
}

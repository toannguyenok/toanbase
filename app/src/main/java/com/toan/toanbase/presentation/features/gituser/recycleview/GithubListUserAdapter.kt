package com.toan.toanbase.presentation.features.gituser.recycleview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.toan.toanbase.R
import com.toan.toanbase.presentation.features.gituser.model.GitUserDetailItemUI

class GithubListUserAdapter() :
    ListAdapter<GitUserDetailItemUI, GithubListViewHolder>(GithubListUserItemDiffCallback()) {

    private var layoutManager: RecyclerView.LayoutManager? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        layoutManager = recyclerView.layoutManager
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        layoutManager = null
        super.onDetachedFromRecyclerView(recyclerView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GithubListViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_user_github, parent, false)
        return GithubListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: GithubListViewHolder, position: Int) {
        holder.bindView(getItem(position))
    }
}

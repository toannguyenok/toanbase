package com.toan.toanbase.presentation.features.movies.model

import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import com.toan.toanbase.domain.movies.model.MovieItemListRes

data class MovieListItemUI(
    val page: Int,
    val totalPage: Int,
    val items: List<MovieItemUI>?
) {
    companion object {
        fun mapFromMovieList(movieItemListRes: BaseMovieListRes<MovieItemListRes>): MovieListItemUI {
            var itemsTemp: MutableList<MovieItemUI>? = null
            movieItemListRes.results?.let { listItem ->
                if (listItem.isNotEmpty()) {
                    itemsTemp = mutableListOf()
                    listItem.forEach { item ->
                        itemsTemp?.add(MovieItemUI.mapFromMovieItemRes(item))
                    }
                }
            }
            return MovieListItemUI(
                page = movieItemListRes.page,
                totalPage = movieItemListRes.totalPages,
                items = itemsTemp
            )
        }

        fun emptyItem(): MovieListItemUI {
            return MovieListItemUI(
                page = 0,
                totalPage = Int.MAX_VALUE,
                items = null
            )
        }
    }
}

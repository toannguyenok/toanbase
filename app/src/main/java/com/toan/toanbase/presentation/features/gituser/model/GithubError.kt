package com.toan.toanbase.presentation.features.gituser.model

import com.toan.toanbase.presentation.features.weather.error.BaseError

class GithubError : BaseError<BaseError.Param>()

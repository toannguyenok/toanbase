package com.toan.toanbase.presentation.features.weather

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.toan.toanbase.R
import com.toan.toanbase.databinding.FragmentWeatherSearchBinding
import com.toan.toanbase.presentation.base.BaseFragment
import com.toan.toanbase.presentation.features.weather.model.WFDailySearchUIState
import com.toan.toanbase.presentation.features.weather.recycleview.WFDailyListAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeatherSearchFragment : BaseFragment<WeatherSearchViewModel>() {
    private val mViewModel by viewModels<WeatherSearchViewModel>()

    companion object {
        fun newInstance(): Fragment {
            return WeatherSearchFragment()
        }
    }

    override fun provideViewModel(): WeatherSearchViewModel {
        return mViewModel
    }

    private lateinit var binding: FragmentWeatherSearchBinding

    private lateinit var wfDailyListAdapter: WFDailyListAdapter
    private val searchTextChange = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            s?.let {
                provideViewModel().search(it.toString())
            }
        }

        override fun afterTextChanged(s: Editable?) {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWeatherSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun setupUI() {
        binding.btnSearch.setOnClickListener {
            provideViewModel().search(binding.searchView.text.toString())
        }
        binding.searchView.addTextChangedListener(searchTextChange)
        setUpRecycleViewWFDaily()
    }

    private fun setUpRecycleViewWFDaily() {
        binding.rvWFDaily.layoutManager = LinearLayoutManager(requireContext())
        wfDailyListAdapter = WFDailyListAdapter()
        binding.rvWFDaily.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        binding.rvWFDaily.adapter = wfDailyListAdapter
    }

    override fun setupViewModel() {
        provideViewModel().initSearch()
        provideViewModel().searchUIState.observe(
            viewLifecycleOwner,
            { state ->
                when (state) {
                    is WFDailySearchUIState.Error -> {
                        // show place holder error
                        binding.tvNetworkError.visibility = View.VISIBLE
                        binding.rvWFDaily.visibility = View.GONE
                        if (state.baseError.isNetworkError()) {
                            binding.tvNetworkError.text =
                                this.resources.getString(R.string.network_error)
                        }
                    }
                    is WFDailySearchUIState.ShowList -> {
                        binding.tvNetworkError.visibility = View.GONE
                        binding.rvWFDaily.visibility = View.VISIBLE
                        wfDailyListAdapter.submitList(state.data.items)
                    }
                    is WFDailySearchUIState.Empty -> {
                        // show place holder
                        binding.tvNetworkError.visibility = View.GONE
                        wfDailyListAdapter.submitList(arrayListOf())
                    }
                    else -> {
                    }
                }
            }
        )
    }
}

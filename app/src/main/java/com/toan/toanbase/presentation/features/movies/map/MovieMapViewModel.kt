package com.toan.toanbase.presentation.features.movies.map

import com.toan.toanbase.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieMapViewModel @Inject constructor() :
    BaseViewModel()

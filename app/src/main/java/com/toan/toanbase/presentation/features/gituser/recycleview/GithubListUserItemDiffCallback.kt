package com.toan.toanbase.presentation.features.gituser.recycleview

import androidx.recyclerview.widget.DiffUtil
import com.toan.toanbase.presentation.features.gituser.model.GitUserDetailItemUI

class GithubListUserItemDiffCallback : DiffUtil.ItemCallback<GitUserDetailItemUI>() {

    override fun areItemsTheSame(oldItem: GitUserDetailItemUI, newItem: GitUserDetailItemUI): Boolean {
        return (oldItem.id == newItem.id)
    }

    override fun areContentsTheSame(oldItem: GitUserDetailItemUI, newItem: GitUserDetailItemUI): Boolean {
        return oldItem.hashCode() == newItem.hashCode()
    }
}

package com.toan.toanbase.presentation.features.weather.recycleview

import androidx.recyclerview.widget.DiffUtil
import com.toan.toanbase.presentation.features.weather.model.WFDetailItemUI

class WFDaiLyItemDiffCallback : DiffUtil.ItemCallback<WFDetailItemUI>() {

    override fun areItemsTheSame(oldItem: WFDetailItemUI, newItem: WFDetailItemUI): Boolean {
        return (oldItem.dt == newItem.dt)
    }

    override fun areContentsTheSame(oldItem: WFDetailItemUI, newItem: WFDetailItemUI): Boolean {
        return oldItem.hashCode() == newItem.hashCode()
    }
}

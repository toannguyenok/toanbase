package com.toan.toanbase.presentation

import androidx.lifecycle.ViewModelProvider
import com.toan.toanbase.presentation.base.BaseActivity
import com.toan.toanbase.presentation.base.BaseViewModel
import com.toan.toanbase.presentation.features.movies.map.MovieMapFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<BaseViewModel>() {

    override fun provideViewModel(): BaseViewModel {
        return ViewModelProvider(this).get(BaseViewModel::class.java)
    }

    override fun setContentView() {
        supportFragmentManager
            .beginTransaction()
            .replace(android.R.id.content, MovieMapFragment.newInstance())
            .commit()
    }

    override fun setupUI() {
    }

    override fun setupViewModel() {
    }
}

package com.toan.toanbase.presentation.features.gituser

import androidx.activity.viewModels
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.toan.toanbase.databinding.ActivityGithubUserSearchBinding
import com.toan.toanbase.presentation.base.BaseActivity
import com.toan.toanbase.presentation.features.gituser.recycleview.GithubListUserAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GitUserSearchActivity : BaseActivity<GitUserSearchViewModel>() {
    private val mViewModel by viewModels<GitUserSearchViewModel>()

    override fun provideViewModel(): GitUserSearchViewModel {
        return mViewModel
    }

    override fun setContentView() {
        binding = ActivityGithubUserSearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private lateinit var binding: ActivityGithubUserSearchBinding
    private lateinit var githubListUserAdapter: GithubListUserAdapter
    private val queryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            query?.let {
                provideViewModel().search(it)
            }
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            newText?.let {
                provideViewModel().search(it)
            }
            return true
        }
    }

    override fun setupUI() {
        binding.searchView.setIconifiedByDefault(true)
        binding.searchView.isFocusable = true
        binding.searchView.isIconified = false
        binding.searchView.requestFocusFromTouch()
        binding.searchView.setOnQueryTextListener(queryTextListener)
        setUpRecycleViewListUser()
    }

    private fun setUpRecycleViewListUser() {
        binding.rvUser.layoutManager = LinearLayoutManager(this)
        githubListUserAdapter = GithubListUserAdapter()
        binding.rvUser.adapter = githubListUserAdapter
    }

    override fun setupViewModel() {
        provideViewModel().initSearch()
        provideViewModel().listQueryUser.observe(
            this,
            { data ->
                githubListUserAdapter.submitList(data.items)
            }
        )
    }
}

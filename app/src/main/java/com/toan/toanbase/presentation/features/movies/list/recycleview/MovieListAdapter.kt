package com.toan.toanbase.presentation.features.movies.list.recycleview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.toan.toanbase.databinding.ItemMovieBinding
import com.toan.toanbase.presentation.features.movies.model.MovieItemUI

class MovieListAdapter(private val itemClick: ((MovieItemUI) -> Unit)?) :
    ListAdapter<MovieItemUI, MovieListViewHolder>(MovieListItemDiffCallback()) {

    private var layoutManager: RecyclerView.LayoutManager? = null

    private var _recyclerView: RecyclerView? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this._recyclerView = recyclerView
        layoutManager = recyclerView.layoutManager
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        layoutManager = null
        this._recyclerView = null
        super.onDetachedFromRecyclerView(recyclerView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val viewHolder = MovieListViewHolder(binding.root, binding)
        viewHolder.itemView.setOnClickListener {
            itemClick?.invoke(getItem(viewHolder.adapterPosition))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
        holder.bindView(getItem(position))
    }

    var pastVisibleItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0

    fun addLoadMoreListener(movieAdapterLoadMoreListener: MovieAdapterLoadMoreListener) {
        _recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > dx) {
                    (layoutManager as? LinearLayoutManager)?.let {
                        visibleItemCount = it.childCount
                        totalItemCount = it.itemCount
                        pastVisibleItems = it.findFirstVisibleItemPosition()
                        if ((visibleItemCount + pastVisibleItems + 2) >= totalItemCount) {
                            movieAdapterLoadMoreListener.onLoadMore()
                        }
                    }
                }
            }
        })
    }
}

package com.toan.toanbase.presentation.features.weather.recycleview

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.toan.toanbase.R
import com.toan.toanbase.presentation.features.weather.model.WFDetailItemUI

class WFDailyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val tvDateValue = view.findViewById<AppCompatTextView>(R.id.tvDateValue)
    private val tvAveTemp = view.findViewById<AppCompatTextView>(R.id.tvAveTempValue)
    private val tvPressureValue = view.findViewById<AppCompatTextView>(R.id.tvPressureValue)
    private val tvHumidityValue = view.findViewById<AppCompatTextView>(R.id.tvHumidityValue)
    private val tvDescriptionValue = view.findViewById<AppCompatTextView>(R.id.tvDescriptionValue)

    fun bindView(wfDetailItemUI: WFDetailItemUI) {
        tvDateValue.text = wfDetailItemUI.getDate()
        tvAveTemp.text = wfDetailItemUI.getAveTemp(itemView.context)
        tvPressureValue.text = wfDetailItemUI.getPressure()
        tvHumidityValue.text = wfDetailItemUI.getHumidity(itemView.context)
        tvDescriptionValue.text = wfDetailItemUI.description.orEmpty()
    }
}

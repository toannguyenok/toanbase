package com.toan.toanbase.presentation.features.movies.list

import android.content.Intent
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import com.toan.toanbase.R
import com.toan.toanbase.databinding.ActivityMovieListBinding
import com.toan.toanbase.presentation.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieListActivity : BaseActivity<MovieListViewModel>() {

    companion object {
        fun newActivity(fragment: Fragment) {
            fragment.startActivity(Intent(fragment.requireContext(), MovieListActivity::class.java))
        }
    }

    private val mViewModel by viewModels<MovieListViewModel>()

    override fun provideViewModel(): MovieListViewModel {
        return mViewModel
    }

    private lateinit var binding: ActivityMovieListBinding
    private val tabs: List<Fragment> by lazy {
        listOf(
            BaseListMovieFragment.newInstance(MovieListType.NOW_PLAYING),
            BaseListMovieFragment.newInstance(MovieListType.TOP_RATED)
        )
    }

    override fun setContentView() {
        binding = ActivityMovieListBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun setupUI() {
        setupPager()
        setupBottomView()
    }

    private fun setupPager() {
        binding.pager.adapter = object :
            FragmentPagerAdapter(supportFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
            override fun getCount(): Int {
                return tabs.size
            }

            override fun getItem(index: Int): Fragment {
                val fragment = getFragmentFromStack(index)
                if (fragment != null) return fragment

                return tabs[index]
            }
        }
    }

    fun getFragmentFromStack(position: Int): Fragment? {
        return supportFragmentManager.findFragmentByTag("android:switcher:${binding.pager.id}:$position")
    }

    private fun setupBottomView() {
        binding.bottomNav.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_now_playing -> {
                    binding.pager.setCurrentItem(0, true)
                }
                R.id.nav_top_rated -> {
                    binding.pager.setCurrentItem(1, true)
                }
            }
            true
        }
    }

    override fun setupViewModel() {
    }
}

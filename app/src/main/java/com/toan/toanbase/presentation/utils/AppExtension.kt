package com.toan.toanbase.presentation.utils

import android.location.Location
import com.google.android.gms.location.FusedLocationProviderClient
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resumeWithException

suspend fun FusedLocationProviderClient.awaitLastLocation(): Location =

    suspendCancellableCoroutine<Location> { continuation ->

        lastLocation.addOnSuccessListener { location ->
            location?.let {
                continuation.resume(location, null)
            }
        }.addOnFailureListener { e ->
            continuation.resumeWithException(e)
        }
    }

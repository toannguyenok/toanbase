package com.toan.toanbase.presentation.features.gituser.model

import com.toan.toanbase.domain.github.model.GithubListUserRes

data class GitUserListUI(
    val totalCount: Long,
    val items: List<GitUserDetailItemUI>?
) {
    companion object {
        fun mapFromGitListUserRes(githubListUserRes: GithubListUserRes): GitUserListUI {
            var itemsTemp: MutableList<GitUserDetailItemUI>? = null
            githubListUserRes.items?.let { listItem ->
                if (listItem.isNotEmpty()) {
                    itemsTemp = mutableListOf()
                    listItem.forEach { item ->
                        itemsTemp?.add(GitUserDetailItemUI.mapFromGitUserRes(item))
                    }
                }
            }
            return GitUserListUI(
                totalCount = githubListUserRes.totalCount,
                items = itemsTemp
            )
        }
    }
}

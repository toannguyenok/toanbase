package com.toan.toanbase.presentation.utils

import com.toan.toanbase.presentation.base.BaseViewModel
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

object RxUtils {

    /**
     * Used when have single observable
     */
    fun <T> applyApiSchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }
}

fun Disposable.add(baseViewModel: BaseViewModel) {
    baseViewModel.addDisposable(this)
}

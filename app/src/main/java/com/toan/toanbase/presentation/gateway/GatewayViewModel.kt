package com.toan.toanbase.presentation.gateway

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.toan.toanbase.AppConfigId
import com.toan.toanbase.domain.movies.usecase.FetchConfigurationUseCase
import com.toan.toanbase.presentation.base.BaseViewModel
import com.toan.toanbase.presentation.utils.RxUtils
import com.toan.toanbase.presentation.utils.SystemUtils
import com.toan.toanbase.presentation.utils.add
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GatewayViewModel @Inject constructor(
    private val systemUtils: SystemUtils,
    private val appConfigId: AppConfigId,
    private val fetchConfigurationUseCase: FetchConfigurationUseCase
) :
    BaseViewModel() {

    private val _isRootState = MutableLiveData<Boolean>()

    var isRootState: MutableLiveData<Boolean>
        get() = _isRootState
        set(value) {
        }

    fun checkRoot() {
        Observable.defer {
            Observable.just(systemUtils.isRooted())
        }.compose(RxUtils.applyApiSchedulers())
            .subscribe(
                {
                    _isRootState.value = it
                },
                {
                    _isRootState.value = false // in case get error => consider as false
                }
            ).add(this)
    }

    fun fetchConfiguration() {
        viewModelScope.launch {
            fetchConfigurationUseCase.invoke(appConfigId.getMovieApiKey())
        }
    }
}

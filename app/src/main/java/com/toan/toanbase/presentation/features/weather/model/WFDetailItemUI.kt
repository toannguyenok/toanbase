package com.toan.toanbase.presentation.features.weather.model

import android.content.Context
import com.toan.toanbase.R
import com.toan.toanbase.domain.weather.model.WeatherForecastDetailRes
import java.text.SimpleDateFormat
import java.util.Date

data class WFDetailItemUI(
    val dt: Long?,
    val aveTemp: Float?,
    val pressure: Int?,
    val humidity: Int?,
    val description: String?
) {
    companion object {
        fun mapFromWFDetail(weatherForecastDetailRes: WeatherForecastDetailRes): WFDetailItemUI {
            var aveTemp: Float? = null
            if (weatherForecastDetailRes.temp?.min != null && weatherForecastDetailRes.temp.max != null) {
                aveTemp =
                    (weatherForecastDetailRes.temp.min + weatherForecastDetailRes.temp.max) / 2f
            }
            return WFDetailItemUI(
                dt = weatherForecastDetailRes.dt,
                aveTemp = aveTemp,
                pressure = weatherForecastDetailRes.pressure,
                humidity = weatherForecastDetailRes.humidity,
                description = weatherForecastDetailRes.weather?.firstOrNull()?.description
            )
        }
    }

    fun getDate(): String {
        dt?.let { source ->
            val formatter = SimpleDateFormat("EEE, d MMM yyyy")
            return formatter.format(Date(source * 1000L))
        }
        return ""
    }

    fun getAveTemp(context: Context): String {
        aveTemp?.let { source ->
            return context.resources.getString(R.string.temperature_template, source)
        }
        return ""
    }

    fun getPressure(): String {
        pressure?.let { source ->
            return source.toString()
        }
        return ""
    }

    fun getHumidity(context: Context): String {
        humidity?.let { source ->
            return context.resources.getString(R.string.humidity_template, source)
        }
        return ""
    }
}

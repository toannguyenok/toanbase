package com.toan.toanbase.presentation.features.gituser.model

import com.toan.toanbase.domain.github.model.GithubDetailUserRes

data class GitUserDetailItemUI(
    val id: Long,
    val profileUrl: String?,
    val name: String?,
    val githubLink: String?
) {
    companion object {
        fun mapFromGitUserRes(githubDetailUserRes: GithubDetailUserRes): GitUserDetailItemUI {
            return GitUserDetailItemUI(
                id = githubDetailUserRes.id,
                profileUrl = githubDetailUserRes.avatarUrl,
                name = githubDetailUserRes.login,
                githubLink = githubDetailUserRes.reposUrl
            )
        }
    }
}

package com.toan.toanbase.domain.movies

import android.util.Log
import com.toan.toanbase.domain.MainCoroutineRule
import com.toan.toanbase.domain.movies.model.MovieItemListRes
import com.toan.toanbase.domain.movies.model.request.GetMovieListRequest
import com.toan.toanbase.domain.movies.usecase.GetMovieNowPlayingUseCase
import com.toan.toanbase.domain.result.Result
import com.toan.toanbase.domain.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.doThrow
import org.mockito.kotlin.given
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.lang.Exception
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After

@RunWith(MockitoJUnitRunner::class)
class GetMovieNowPlayingUseCaseTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutineRule = MainCoroutineRule()

    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    private fun generateMovieItemListRes(
        id: Int,
        title: String
    ): MovieItemListRes {
        return MovieItemListRes(
            id = id,
            title = title,
            posterPath = null,
            adult = true,
            overView = null,
            releaseDate = null,
            originalLanguage = null,
            originalTitle = null,
            backdropPath = null,
            popularity = null,
            voteCount = null,
            voteAverage = null
        )
    }

//    @Test
//    fun `TC execute usecase get exception`() = runBlocking {
//        val getMovieListRequest = GetMovieListRequest("1",1)
//        val mock = mock<MovieRepository>()
//        given(mock.getMovieNowPlaying(getMovieListRequest)).willAnswer{ Throwable()}
//        val useCase = GetMovieNowPlayingUseCase(mock)
//        val testResults = mutableListOf<Result<BaseMovieListRes<MovieItemListRes>>>()
//
//        val job = launch {
//            useCase.invoke(getMovieListRequest).stateIn(this).toList(testResults)
//        }
//
//        assertEquals(1, testResults.size)
//        job.join()
//    }

}

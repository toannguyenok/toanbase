package com.toan.toanbase.domain.weather

import com.nhaarman.mockitokotlin2.whenever
import com.toan.toanbase.domain.result.Result
import com.toan.toanbase.domain.weather.model.WeatherForecastDetailRes
import com.toan.toanbase.domain.weather.model.WeatherForecastListRes
import com.toan.toanbase.domain.weather.model.request.GetWeatherForecastRequest
import com.toan.toanbase.domain.weather.usecase.GetWeatherForeCastUseCase
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetWeatherForeCastUseCaseTest {
    @Mock
    private lateinit var wfRepository: WeatherRepository

    @InjectMocks
    private lateinit var useCaseTest: GetWeatherForeCastUseCase

    @Before
    fun setup() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setSingleSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun `TC execute with query empty, then return Result empty`() {
        val requestEmpty = GetWeatherForecastRequest(
            query = "",
            cnt = 7,
            appId = "abc",
            units = "abc",
            timeQuery = 1111L
        )
        val testObserver: TestObserver<Result<WeatherForecastListRes>> =
            useCaseTest.invoke(requestEmpty).test()
        testObserver.assertValueCount(1)
        testObserver.assertValue { value ->
            when (value) {
                is Result.Success -> {
                    if (value.data.list == null) {
                        return@assertValue true
                    }
                    return@assertValue false
                }
                else -> return@assertValue false
            }
        }
        testObserver.dispose()
    }

    private fun generateWFDailyDetail(
        dt: Long,
        pressure: Int
    ): WeatherForecastDetailRes {
        return WeatherForecastDetailRes(
            dt = dt,
            pressure = pressure,
            humidity = null,
            temp = null,
            weather = null
        )
    }

    @Test
    fun `TC execute with query available, then return Result with list by repository`() {
        val requestEmpty = GetWeatherForecastRequest(
            query = "abc",
            cnt = 7,
            appId = "abc",
            units = "abc",
            timeQuery = 1111L
        )
        val fakeItem1 = generateWFDailyDetail(1L, 1)
        val fakeItem2 = generateWFDailyDetail(2L, 2)
        val weatherForecastListRes = WeatherForecastListRes(7, arrayListOf(fakeItem1, fakeItem2))
        whenever(
            wfRepository.getWeatherForecast(requestEmpty)
        ).thenReturn(
            Observable.just(weatherForecastListRes)
        )
        val testObserver: TestObserver<Result<WeatherForecastListRes>> =
            useCaseTest.invoke(requestEmpty).test()
        testObserver.assertValueCount(1)
        testObserver.assertValue { value ->
            when (value) {
                is Result.Success -> {
                    if (value.data.list?.size == 2) {
                        val firstData = value.data.list?.getOrNull(0)
                        val secondData = value.data.list?.getOrNull(1)
                        if (firstData?.pressure == 1 && secondData?.pressure == 2) {
                            return@assertValue true
                        }
                    }
                    return@assertValue false
                }
                else -> return@assertValue false
            }
        }
        testObserver.dispose()
    }
}

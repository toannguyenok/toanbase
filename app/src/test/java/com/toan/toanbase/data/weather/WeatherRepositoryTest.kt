package com.toan.toanbase.data.weather

import com.nhaarman.mockitokotlin2.whenever
import com.toan.toanbase.data.weather.local.database.WFDailyDataSource
import com.toan.toanbase.data.weather.remote.WeatherService
import com.toan.toanbase.data.weather.repository.WeatherRepositoryImpl
import com.toan.toanbase.domain.weather.model.WeatherForecastDetailRes
import com.toan.toanbase.domain.weather.model.WeatherForecastListRes
import com.toan.toanbase.domain.weather.model.request.GetWeatherForecastRequest
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherRepositoryTest {
    @Mock
    private lateinit var weatherService: WeatherService

    @Mock
    private lateinit var wfDailyDataSource: WFDailyDataSource

    @InjectMocks
    private lateinit var repository: WeatherRepositoryImpl

    @Before
    fun setup() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setSingleSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    private fun generateWFDailyDetail(
        dt: Long,
        pressure: Int
    ): WeatherForecastDetailRes {
        return WeatherForecastDetailRes(
            dt = dt,
            pressure = pressure,
            humidity = null,
            temp = null,
            weather = null
        )
    }

    @Test
    fun `TC execute with cache null, then return result from remote`() {
        val request = GetWeatherForecastRequest(
            query = "abc",
            cnt = 7,
            appId = "abc",
            units = "abc",
            timeQuery = 1111L
        )
        val fakeItem1 = generateWFDailyDetail(1L, 1)
        val fakeItem2 = generateWFDailyDetail(2L, 2)
        val weatherForecastListRes = WeatherForecastListRes(7, arrayListOf(fakeItem1, fakeItem2))
        whenever(
            wfDailyDataSource.queryWFDailyCache(request.query, request.cnt, request.timeQuery)
        ).thenReturn(
            null
        )
        whenever(
            weatherService.getForeCastDaily(
                request.query,
                request.cnt,
                request.appId,
                request.units
            )
        ).thenReturn(
            Observable.just(weatherForecastListRes)
        )
        val testObserver: TestObserver<WeatherForecastListRes> =
            repository.getWeatherForecast(request).test()
        testObserver.assertValueCount(1)
        testObserver.assertValue { value ->
            if (value.list?.size == 2) {
                val firstData = value.list?.getOrNull(0)
                val secondData = value.list?.getOrNull(1)
                if (firstData?.pressure == 1 && secondData?.pressure == 2) {
                    return@assertValue true
                }
            }
            return@assertValue false
        }
        testObserver.dispose()
    }

    @Test
    fun `TC execute with cache available, then return result from cache`() {
        val request = GetWeatherForecastRequest(
            query = "abc",
            cnt = 7,
            appId = "abc",
            units = "abc",
            timeQuery = 1111L
        )
        val fakeItem1 = generateWFDailyDetail(1L, 1)
        val fakeItem2 = generateWFDailyDetail(2L, 2)
        val weatherForecastListRes = WeatherForecastListRes(7, arrayListOf(fakeItem1, fakeItem2))

        whenever(
            wfDailyDataSource.queryWFDailyCache(request.query, request.cnt, request.timeQuery)
        ).thenReturn(
            weatherForecastListRes
        )
        val testObserver: TestObserver<WeatherForecastListRes> =
            repository.getWeatherForecast(request).test()
        testObserver.assertValueCount(1)
        testObserver.assertValue { value ->
            if (value.list?.size == 2) {
                val firstData = value.list?.getOrNull(0)
                val secondData = value.list?.getOrNull(1)
                if (firstData?.pressure == 1 && secondData?.pressure == 2) {
                    return@assertValue true
                }
            }
            return@assertValue false
        }
        testObserver.dispose()
    }
}

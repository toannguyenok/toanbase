package com.toan.toanbase.data.movies

import com.nhaarman.mockitokotlin2.times
import com.toan.toanbase.data.movies.local.database.ConfigurationImageDataSource
import com.toan.toanbase.data.movies.remote.MovieService
import com.toan.toanbase.data.movies.repository.MovieRepositoryImpl
import com.toan.toanbase.domain.movies.model.BaseMovieListRes
import com.toan.toanbase.domain.movies.model.ConfigurationImageRes
import com.toan.toanbase.domain.movies.model.ConfigurationRes
import com.toan.toanbase.domain.movies.model.MovieItemListRes
import com.toan.toanbase.domain.movies.model.request.GetMovieListRequest
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@RunWith(MockitoJUnitRunner::class)
class MovieRepositoryTest {

    @Mock
    private lateinit var configurationImageDataSource: ConfigurationImageDataSource

    @Before
    fun setup() {
    }

    private fun generateMovieItemListRes(
        id: Int,
        title: String
    ): MovieItemListRes {
        return MovieItemListRes(
            id = id,
            title = title,
            posterPath = null,
            adult = true,
            overView = null,
            releaseDate = null,
            originalLanguage = null,
            originalTitle = null,
            backdropPath = null,
            popularity = null,
            voteCount = null,
            voteAverage = null
        )
    }

    @Test
    fun `TC get movie now playing, then return result from remote null`() {
        runBlocking {
            val movieListRequest = GetMovieListRequest("1", 1)
            val mock = mock<MovieService>() {
                onBlocking { getMovieNowPlaying("1", 1) } doReturn BaseMovieListRes(
                    1,
                    null,
                    10,
                    100
                )
            }

            val movieRepositoryImpl = MovieRepositoryImpl(mock, configurationImageDataSource)

            val result = movieRepositoryImpl.getMovieNowPlaying(movieListRequest)
            Assert.assertEquals(null, result.results)
        }
    }

    @Test
    fun `TC get movie now playing, then return result from remote empty`() {
        runBlocking {
            val movieListRequest = GetMovieListRequest("1", 1)
            val mock = mock<MovieService>() {
                onBlocking { getMovieNowPlaying("1", 1) } doReturn BaseMovieListRes(
                    1,
                    arrayListOf(),
                    10,
                    100
                )
            }

            val movieRepositoryImpl = MovieRepositoryImpl(mock, configurationImageDataSource)

            val result = movieRepositoryImpl.getMovieNowPlaying(movieListRequest)
            Assert.assertEquals(0, result.results?.size)
        }
    }

    @Test
    fun `TC get movie now playing, then return result from remote`() {
        runBlocking {
            val movieListRequest = GetMovieListRequest("1", 1)
            val item1 = generateMovieItemListRes(1, "fake1")
            val item2 = generateMovieItemListRes(2, "fake2")
            val mock = mock<MovieService>() {
                onBlocking { getMovieNowPlaying("1", 1) } doReturn BaseMovieListRes(
                    1,
                    listOf(item1, item2),
                    10,
                    100
                )
            }

            val movieRepositoryImpl = MovieRepositoryImpl(mock, configurationImageDataSource)

            val result = movieRepositoryImpl.getMovieNowPlaying(movieListRequest)
            Assert.assertEquals(2, result.results?.size)
            Assert.assertEquals(1, result.results?.getOrNull(0)?.id)
            Assert.assertEquals(2, result.results?.getOrNull(1)?.id)
        }
    }

    @Test
    fun `TC get movie top rated, then return result from remote null`() {
        runBlocking {
            val movieListRequest = GetMovieListRequest("1", 1)
            val mock = mock<MovieService>() {
                onBlocking { getMovieTopRated("1", 1) } doReturn BaseMovieListRes(
                    1,
                    null,
                    10,
                    100
                )
            }

            val movieRepositoryImpl = MovieRepositoryImpl(mock, configurationImageDataSource)

            val result = movieRepositoryImpl.getMovieTopRated(movieListRequest)
            Assert.assertEquals(null, result.results)
        }
    }

    @Test
    fun `TC get movie top rated, then return result from remote empty`() {
        runBlocking {
            val movieListRequest = GetMovieListRequest("1", 1)
            val mock = mock<MovieService>() {
                onBlocking { getMovieTopRated("1", 1) } doReturn BaseMovieListRes(
                    1,
                    arrayListOf(),
                    10,
                    100
                )
            }

            val movieRepositoryImpl = MovieRepositoryImpl(mock, configurationImageDataSource)

            val result = movieRepositoryImpl.getMovieTopRated(movieListRequest)
            Assert.assertEquals(0, result.results?.size)
        }
    }

    @Test
    fun `TC get movie top rated, then return result from remote`() {
        runBlocking {
            val movieListRequest = GetMovieListRequest("1", 1)
            val item1 = generateMovieItemListRes(1, "fake1")
            val item2 = generateMovieItemListRes(2, "fake2")
            val mock = mock<MovieService>() {
                onBlocking { getMovieTopRated("1", 1) } doReturn BaseMovieListRes(
                    1,
                    listOf(item1, item2),
                    10,
                    100
                )
            }

            val movieRepositoryImpl = MovieRepositoryImpl(mock, configurationImageDataSource)

            val result = movieRepositoryImpl.getMovieTopRated(movieListRequest)
            Assert.assertEquals(2, result.results?.size)
            Assert.assertEquals(1, result.results?.getOrNull(0)?.id)
            Assert.assertEquals(2, result.results?.getOrNull(1)?.id)
        }
    }

    @Test
    fun `TC fetch configuration, config available then datasource save 1 time`() {
        runBlocking {
            val configurationImageRes = ConfigurationImageRes(
                "base",
                null,
                null,
                null
            )
            val mock = mock<MovieService>() {
                onBlocking { getConfiguration("1") } doReturn ConfigurationRes(
                    configurationImageRes
                )
            }

            val movieRepositoryImpl = MovieRepositoryImpl(mock, configurationImageDataSource)

            movieRepositoryImpl.fetchConfiguration("1")
            verify(configurationImageDataSource, times(1)).saveConfigurationImageCache(
                configurationImageRes
            )
        }
    }

    @Test
    fun `TC fetch configuration, config null then datasource save 0 time`() {
        runBlocking {
            val mock = mock<MovieService>() {
                onBlocking { getConfiguration("1") } doReturn ConfigurationRes(
                    null
                )
            }

            val movieRepositoryImpl = MovieRepositoryImpl(mock, configurationImageDataSource)

            movieRepositoryImpl.fetchConfiguration("1")
            verify(configurationImageDataSource, times(0)).saveConfigurationImageCache(
                ConfigurationImageRes("base", null, null, null)
            )
        }
    }
}
